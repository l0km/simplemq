package gu.simplemq.mqtt;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.gitee.l0km.com4j.basex.bean.BeanPropertySupport.BEAN_SUPPORT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.ISubscriber;
import gu.simplemq.MQProperties;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;
import gu.simplemq.SimplemqContext;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class MqttSubscriberTest implements MqttConstants{
	private static final String MQTT_PORT = Integer.toString(DEFAULT_MQTT_PORT);
	
    private static int qos = 2;

	private static String host;
	private static Integer port;
//    private static String userName = "tuyou";
//    private static String passWord = "tuyou";
   
    private static void configureSsl(MqttConnectOptions connOpts) {
        try {
            Properties sslProperties = new Properties();

            // 设置信任库路径和密码
            sslProperties.setProperty("com.ibm.ssl.trustStore", "J:\\simplemq\\simplemq-mqtt\\client.ts");
            sslProperties.setProperty("com.ibm.ssl.trustStorePassword", "facelib");

            // 设置密钥库路径和密码（如果需要客户端认证）
//            sslProperties.setProperty("com.ibm.ssl.keyStore", "J:\\simplemq\\simplemq-mqtt\\client.ks");
//            sslProperties.setProperty("com.ibm.ssl.keyStorePassword", "facelib");

            // 设置 SSL 协议
            sslProperties.setProperty("com.ibm.ssl.protocol", "TLSv1.2");

            // 设置 SSL 属性
            connOpts.setSSLProperties(sslProperties);
            
            connOpts.setHttpsHostnameVerificationEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static MqttClient connect(String clientId, String broker, boolean ssl) throws MqttException{
    	MemoryPersistence persistence = new MemoryPersistence();
    	MqttConnectOptions connOpts = new MqttConnectOptions();
    	if(ssl) {
    		configureSsl(connOpts);
    	}
    	connOpts.setCleanSession(false);
    	//        connOpts.setUserName(userName);
    	//        connOpts.setPassword(passWord.toCharArray());
    	connOpts.setConnectionTimeout(10);
    	connOpts.setKeepAliveInterval(20);
    	//        connOpts.setServerURIs(uris);
    	//        connOpts.setWill(topic, "close".getBytes(), 2, true);
    	MqttClient mqttClient = new MqttClient(broker, clientId, persistence);
    	mqttClient.connect(connOpts);
    	return mqttClient;
    }
    
    public static void sub(MqttClient mqttClient,String topic) throws MqttException{
        int[] Qos  = {qos};
        String[] topics = {topic};
        mqttClient.subscribe(topics, Qos);
    }    
    

	private static void waitquit(){
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		try{
			while(!"quit".equalsIgnoreCase(reader.readLine())){
			}
			System.exit(0);
		} catch (IOException e) {
	
		}finally {
	
		}
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		host = firstNonNull(System.getProperty("host"),DEFAULT_MQTT_HOST);
		port = Integer.valueOf(firstNonNull(System.getProperty("port"),MQTT_PORT));
	}
	@Test
	public void test1() {
		MqttClient mqttClient;
		try {
			mqttClient = connect("testSub", "tcp://" + DEFAULT_MQTT_HOST + ":" + DEFAULT_MQTT_PORT, false);
	        mqttClient.setCallback(new MyCallback());

			sub(mqttClient,"ActiveMQ.Advisory.Consumer.Topic.chat1");
			sub(mqttClient,"chat1");
			sub(mqttClient,"chat2");
			sub(mqttClient,"chat3");
			waitquit();
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}

	}
	@Test
	public void test1SSL() {
		MqttClient mqttClient;
		try {
			mqttClient = connect("testSub", "ssl://" + DEFAULT_MQTT_HOST + ":" + 1884, true);
			mqttClient.setCallback(new MyCallback());
			
			sub(mqttClient,"ActiveMQ.Advisory.Consumer.Topic.chat1");
			sub(mqttClient,"chat1");
			sub(mqttClient,"chat2");
			sub(mqttClient,"chat3");
			waitquit();
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}
		
	}

	@Test
	public void test2() {
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, MQTT_SCHEMA + "://user:password@" + DEFAULT_MQTT_HOST + ":" + DEFAULT_MQTT_PORT);

		final IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try{
			assertTrue(factory.testConnect());
			final Channel<String> c1 = new LogChannel("chat1");
			final Channel<String> c2 = new LogChannel("chat2");
			factory.getSubscriber().register(c1,c2);
			waitquit();
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void test2SSL() {
		ImmutableMap<String,String> m = 
				ImmutableMap.of(
						MQ_URI, "ssl" + "://user:password@" + host + ":" + 1884,
						"com.ibm.ssl.trustStore", "J:\\simplemq\\simplemq-mqtt\\client.ts",
						"com.ibm.ssl.trustStorePassword", "facelib",
						"com.ibm.ssl.protocol", "TLSv1.2");
		final IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try{
			
			final Channel<String> c1 = new LogChannel("chat1");
			final Channel<String> c2 = new LogChannel("chat1");
			factory.getSubscriber().register(c1,c2);
			Runtime.getRuntime().addShutdownHook(new Thread(){
				
				@Override
				public void run() {
					logger.info("unregister:ch1,ch2");
					factory.getSubscriber().unregister(c1,c2);
				}
				
			});
			waitquit();
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
		
	}
	/**
	 * 可以通过命令行提供连接参数执行订阅消息的测试
	 */
	@Test
	public void subscribeTest() {
		Builder<String,String> builder = ImmutableMap.builder();
		String username = System.getProperty("username", "");
		if(!username.isEmpty())
		{
			builder.put(MQ_USERNAME, username);
			String password = System.getProperty("password", "");
			builder.put(MQ_PASSWORD, password);
		}
		String host = System.getProperty("host", "");
		if(!host.isEmpty()){
			builder.put(MQ_HOST, host);
		}
		String port = System.getProperty("port", "");
		if(!port.isEmpty())
		{
			builder.put(MQ_PORT, port);
		}
		ImmutableMap<String,String> m = builder.build();
		String channel =  System.getProperty("channel", "c1");
		final IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.PAHO).init(m);
		try{

			final Channel<String> c1 = new LogChannel(channel);
			factory.getSubscriber().register(c1);
			Runtime.getRuntime().addShutdownHook(new Thread(){

				@Override
				public void run() {
					logger.info("unregister:ch1,ch2");
					factory.getSubscriber().unregister(c1);
				}

			});
			waitquit();
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void test4CheckConnect() {
		ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI, "tcp://localhost:1883");
		MQProperties props = PropertiesHelper.MHELPER.asMQProperties(m);
		boolean connectable = MqttPoolLazys.NAMED_POOLS.testConnect(props, null);
		logger.info("{} connectable {}", props.getLocation(), connectable);
		assertTrue(connectable);
	}
	/**
	 * 定义连接池测试
	 */
	@Test
	public void test5DefinePool() {
		ImmutableMap<String, ?> props = ImmutableMap.<String, Object>of(MQ_URI, "tcp://localhost:1883");
		try (IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.PAHO).init(props)) {
			/** 在 props的基础上，增加额外的连接选项 */
			ImmutableMap<String, ?> other = ImmutableMap.<String, Object>builder()
					.putAll(props)
					.put(MQTT_clientId, "guyadong-pc")
					/** 定义连接池时，指定额外的连接选项 */
					.put(MQTT_connOpts_cleanSession, false).build();
			// 定义连接池
			factory.definePool("other", other);
			// 获取默认连接池的订阅者
			ISubscriber subscriber = factory.getSubscriber();
			testSubscriber(subscriber, true);
			// 获取指定连接池的订阅者
			ISubscriber otherSubscriber = factory.getSubscriber("other");
			logger.info("subscriber class {}", otherSubscriber.getClass().getName());
			otherSubscriber.register(new LogChannel("chat1"));
//			otherSubscriber.unsubscribe("chat1");
			// 两个订阅者对象不应该是同一个对象
			assertNotEquals(subscriber, otherSubscriber);
			assertTrue(otherSubscriber.getClass().getPackage().getName().startsWith("gu.simplemq.mqtt"));
			testSubscriber(otherSubscriber, false);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}
	}
	private void testSubscriber(ISubscriber subscriber, boolean exepctCleanSession) {
		logger.info("================");
		logger.info("subscriber {}",subscriber);
		logger.info("pool.canonicalURI {}",BEAN_SUPPORT.getProperty(subscriber, "pool.canonicalURI"));
		logger.info("pool.properties {}",BEAN_SUPPORT.getProperty(subscriber, "pool.properties"));
		logger.info("pool.connOpts.cleanSession {}",BEAN_SUPPORT.getProperty(subscriber, "pool.connOpts.cleanSession"));
		// 断言连接池的 cleanSession 是否与预期一致
		assertEquals(exepctCleanSession, BEAN_SUPPORT.getProperty(subscriber, "pool.connOpts.cleanSession"));
	}
	class MyCallback implements MqttCallback{
		@Override
		public void connectionLost(Throwable cause) {
			// 连接丢失后，一般在这里面进行重连
			System.out.println("连接断开，可以做重连");
		}
		@Override
		public void deliveryComplete(IMqttDeliveryToken token) {
			logger.info("deliveryComplete---------" + token.isComplete());
		}
		@Override
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			logger.info("接收消息的主题 : " + topic);
			logger.info("接收消息的质量Qos : " + message.getQos());

			String msg = new String(message.getPayload());
			logger.info("msg:" + msg);
		}
		
	}
	class LogChannel extends Channel<String>{

		protected LogChannel(String name) {
			super(name);
		}
		@Override
		public void onSubscribe(String t,SimplemqContext context) throws SmqUnsubscribeException {
			logger.info(name + " msg:" + t);
		}
	}
}
