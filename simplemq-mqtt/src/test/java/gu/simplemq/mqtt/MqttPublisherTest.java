package gu.simplemq.mqtt;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Properties;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * @author guyadong
 *
 */
public class MqttPublisherTest implements MqttConstants{
	private static final String MQTT_HOST = "localhost";
	private static final String MQTT_PORT = Integer.toString(DEFAULT_MQTT_PORT);
	private static String host;
	private static int port;
	
    private static int qos = 2;
//    private static String userName = "tuyou";
//    private static String passWord = "tuyou";
   
    private static void configureSsl(MqttConnectOptions connOpts) {
        try {
            Properties sslProperties = new Properties();

            // 设置信任库路径和密码
            sslProperties.setProperty("com.ibm.ssl.trustStore", "J:\\simplemq\\simplemq-mqtt\\client.ts");
            sslProperties.setProperty("com.ibm.ssl.trustStorePassword", "facelib");

            // 设置密钥库路径和密码（如果需要客户端认证）
//            sslProperties.setProperty("com.ibm.ssl.keyStore", "J:\\simplemq\\simplemq-mqtt\\client.ks");
//            sslProperties.setProperty("com.ibm.ssl.keyStorePassword", "facelib");

            // 设置 SSL 协议
            sslProperties.setProperty("com.ibm.ssl.protocol", "TLSv1.2");

            // 设置 SSL 属性
            connOpts.setSSLProperties(sslProperties);
            
            connOpts.setHttpsHostnameVerificationEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static MqttClient connect(String clientId, String broker, boolean ssl) throws MqttException{
    	MemoryPersistence persistence = new MemoryPersistence();
    	MqttConnectOptions connOpts = new MqttConnectOptions();
    	if(ssl) {
    		configureSsl(connOpts);
    	}
    	connOpts.setCleanSession(false);
    	//        connOpts.setUserName(userName);
    	//        connOpts.setPassword(passWord.toCharArray());
    	connOpts.setConnectionTimeout(10);
    	connOpts.setKeepAliveInterval(20);
    	//        connOpts.setServerURIs(uris);
    	//        connOpts.setWill(topic, "close".getBytes(), 2, true);
    	MqttClient mqttClient = new MqttClient(broker, clientId, persistence);
    	mqttClient.connect(connOpts);
    	return mqttClient;
    }
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		host = firstNonNull(System.getProperty("host"),MQTT_HOST);
		port = Integer.valueOf(firstNonNull(System.getProperty("port"),MQTT_PORT));
	}
	@Test
	public void test() throws InterruptedException {
		Channel<Date> chat1 = new Channel<Date>("chat1",Date.class);
		IPublisher publisher = MqttFactory.MQ_INSTANCE_SUPPLIER.getPublisher(MqttPoolLazys.NAMED_POOLS.createPool(null));
		for(int i=0;i<100;++i){
			try {
				
				Date date = new Date();
				publisher.publish(chat1, date);
				logger.info(date.getTime() +" : " +date.toString());
			} catch (Exception e) {
				logger.info(e.getMessage());
			}
			Thread.sleep(2000);
		}
	}
	@Test
	public void test1() throws InterruptedException {
		MqttClient mqttClient;
		try {
			mqttClient = connect("testPub", "tcp://" + MQTT_HOST + ":" + DEFAULT_MQTT_PORT, true);
			for(int i=0;i<100;++i){
				try {
					
					Date date = new Date();
					mqttClient.publish("chat1",date.toString().getBytes(Charset.forName("UTF-8")),qos,false);
					logger.info(date.getTime() +" : " +date.toString());
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
				Thread.sleep(2000);
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}
	}
	@Test
	public void test1SSL() throws InterruptedException {
		MqttClient mqttClient;
		try {
			mqttClient = connect("testPub", "ssl://" + MQTT_HOST + ":" + 1884, true);
			for(int i=0;i<100;++i){
				try {
					
					Date date = new Date();
					mqttClient.publish("chat1",date.toString().getBytes(Charset.forName("UTF-8")),qos,false);
					logger.info(date.getTime() +" : " +date.toString());
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
				Thread.sleep(2000);
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}
	}

	@Test
	public void test2() throws Exception{
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, MQTT_SCHEMA + "://user:password@" + host + ":" + port);
		final IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		IPublisher publisher = factory.getPublisher();
		Channel<String> c1 = new Channel<String>("chat1"){};
		Channel<String> c2 = new Channel<String>("chat2"){};
		for(int i=0;i<100;++i){
			Date date = new Date();
			publisher.publish(c1, "MQTT " + date.toString());
			publisher.publish(c2, "MQTT " + date.toString());
			logger.info(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
		factory.close();
	}

	@Test
	public void test2SSL() throws Exception{
		try {
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
			ImmutableMap<String,String> m = 
					ImmutableMap.of(
							MQ_URI, "ssl" + "://user:password@" + host + ":" + 1884,
							"com.ibm.ssl.trustStore", "J:\\simplemq\\simplemq-mqtt\\client.ts",
							"com.ibm.ssl.trustStorePassword", "facelib",
							"com.ibm.ssl.protocol", "TLSv1.2");
			final IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.PAHO).init(m);
			IPublisher publisher = factory.getPublisher();
			Channel<String> c1 = new Channel<String>("chat1"){};
			Channel<String> c2 = new Channel<String>("chat2"){};
			for(int i=0;i<100;++i){
				Date date = new Date();
				publisher.publish(c1, "MQTT " + date.toString());
				publisher.publish(c2, "MQTT " + date.toString());
				logger.info(date.getTime() +" : " +date.toString());
				Thread.sleep(2000);
			}
			factory.close();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
