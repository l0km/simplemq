package gu.simplemq.mqtt;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IProducer;
import gu.simplemq.MessageQueueFactorys;

/**
 * @author guyadong
 *
 */
public class MqttProducerTest implements MqttConstants{

	@Test
	public void test2() throws InterruptedException, IOException {
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, MQTT_SCHEMA + "://user:password@" + DEFAULT_MQTT_HOST + ":" + DEFAULT_MQTT_PORT);

		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		IProducer producer = factory.getProducer();
		Channel<Date> datech = new Channel<Date>("datequeue",Date.class);
		for(int i=0; i<100; ){
			Date date = new Date();
			try{
				producer.produce(datech,date);
				++i;
				logger.info("" + i + ":" + date.getTime() +" : " +date.toString());
			}catch (Throwable e) {
				e.printStackTrace();
			}
			Thread.sleep(2000);
		}
	}
}
