package gu.simplemq.mqtt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IConsumer;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.IMessageAdapterExt;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.SimplemqContext;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class MqttConsumerTest implements MqttConstants {
	private static void waitquit() {
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			while (!"quit".equalsIgnoreCase(reader.readLine())) {
			}
			System.exit(0);
		} catch (IOException e) {

		} finally {

		}
	}

	@Test
	public void test1() throws IOException {
//		ImmutableMap<String, String> m = ImmutableMap.of(MQ_USERNAME, "user", MQ_PASSWORD, "password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, MQTT_SCHEMA + "://user:password@" + DEFAULT_MQTT_HOST + ":" + DEFAULT_MQTT_PORT);
		try (IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false)) {
			IConsumer consumer = factory.getConsumer();
			Channel<String> list1 = new Channel<String>("list1", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list1", t);
				}
			});
			Channel<String> list2 = new Channel<String>("list2", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list2", t);
				}
			});
			Channel<String> list3 = new Channel<String>("list3", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list3", t);
				}
			});
			consumer.register(list1, list2);
			consumer.register(list3);
			waitquit();
			consumer.unregister(list1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test3() {
		final AtomicInteger count = new AtomicInteger(0);
		ImmutableMap<String, String> m = ImmutableMap.of(MQ_USERNAME, "user", MQ_PASSWORD, "password");
		try (IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false)) {
			IConsumer consumer = factory.getConsumer();
			Channel<Date> datech = new Channel<Date>("datequeue", Date.class, new IMessageAdapterExt.Adapter<Date>() {

				@Override
				public void onSubscribe(Date t, SimplemqContext context) throws SmqUnsubscribeException {
					logger.info("consum {}:{},{}", "datequeue", count.incrementAndGet(), t);
					context.acknowledge();
				}
			}).setAutoack(false);
			consumer.register(datech);
			logger.info("register {}", datech);
			waitquit();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
