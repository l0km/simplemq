package gu.simplemq.mqtt;

import gu.simplemq.MQInstanceFactory;

/**
 * 基于{@link MQInstanceFactory}实现 MQTT消息处理对象管理<br>
 * @author guyadong
 *
 */
public class MqttFactory extends MQInstanceFactory<MqttSubscriber,MqttProducer,MqttSubscriber,MqttPublisher,MqttPoolLazy>{
	public static final MqttFactory MQ_INSTANCE_SUPPLIER = new MqttFactory();
	MqttFactory() {}
}
