package gu.simplemq.mqtt;

import java.util.Map;
import gu.simplemq.MQProperties;
import gu.simplemq.pool.NamedMQPools;

/**
 * 基于名字管理的 {@link org.eclipse.paho.client.mqttv3.MqttClient}资源池（线程安全）<br>
 * @author guyadong
 *
 */
public class MqttPoolLazys extends NamedMQPools<MqttPoolLazy>{
	public static final MqttPoolLazys NAMED_POOLS = new MqttPoolLazys();
	protected MqttPoolLazys() {
		super(PropertiesHelper.MHELPER);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public void checkConnect(Map input, Long timeoutMills) {
		MQProperties props = propertiesHelper.initParameters(input);
		if(timeoutMills != null && timeoutMills > 0){
			/** 定义 MqttPoolLazy.timeTowaitInMillis 字段 */
			props.put("timeTowaitInMillis", timeoutMills);
		}
		checkConnect0(props);
	}
}
