package gu.simplemq.mqtt;

import gu.simplemq.Constant;

/**
 * MQTT 常量定义
 * @author guyadong
 *
 */
public interface MqttConstants extends Constant {
	
	public static final String MQTT_serverURI = "serverURI";
	public static final String MQTT_clientId = "clientId";	
	
	public static final String MQTT_qos = "qos";
	
	public static final String CONNOPTS_PREFIX = "connOpts.";
	public static final String MQTT_connOpts_keepAliveInterval ="connOpts.keepAliveInterval";
	public static final String MQTT_connOpts_maxInflight ="connOpts.maxInflight";
	public static final String MQTT_connOpts_username ="connOpts.userName";
	public static final String MQTT_connOpts_password ="connOpts.password";
	public static final String MQTT_connOpts_socketFactory ="connOpts.socketFactory";
	public static final String MQTT_connOpts_sslClientProps ="connOpts.sslClientProps";
	public static final String MQTT_connOpts_httpsHostnameVerificationEnabled ="connOpts.httpsHostnameVerificationEnabled";
	public static final String MQTT_connOpts_sslHostnameVerifier ="connOpts.sslHostnameVerifier";
	public static final String MQTT_connOpts_cleanSession ="connOpts.cleanSession";
	public static final String MQTT_connOpts_connectionTimeout ="connOpts.connectionTimeout";
	public static final String MQTT_connOpts_serverURIs ="connOpts.serverURIs";
	public static final String MQTT_connOpts_mqttVersion ="connOpts.mqttVersion";
	public static final String MQTT_connOpts_automaticReconnect ="connOpts.automaticReconnect";
	public static final String MQTT_connOpts_maxReconnectDelay ="connOpts.maxReconnectDelay";
	public static final String MQTT_connOpts_customWebSocketHeaders ="connOpts.customWebSocketHeaders";
	public static final String MQTT_connOpts_executorServiceTimeout ="connOpts.executorServiceTimeout";
	
	public static final String DEFAULT_MQTT_HOST = "localhost";
	/** paho 的MQTT 协议的默认schema */
	public static final String PAHO_MQTT_SCHEMA = "tcp";
	/** paho 的MQTT SSL协议的schema */
	public static final String PAHO_MQTT_SSL_SCHEMA = "ssl";
	
	public static final String DEFAULT_MQTT_URI = PAHO_MQTT_SCHEMA + "://" + DEFAULT_MQTT_HOST + ":" + DEFAULT_MQTT_PORT;

	public static final int DEFAULT_QOS = 2;

    
}
