package gu.simplemq.mqtt;

import gu.simplemq.BaseMessageQueueFactory;
import gu.simplemq.MQInstanceSupplier;
import gu.simplemq.pool.NamedMQPools;

import static gu.simplemq.mqtt.PropertiesHelper.MHELPER;

/**
 * {@link gu.simplemq.IMessageQueueFactory}接口的MQTT实现类
 * @author guyadong
 *
 */
public final class MessageQueueFactoryImpl extends BaseMessageQueueFactory<MqttPoolLazy>{
	private final MQInstanceSupplier<MqttPoolLazy> mqInstanceFactory;
	private final NamedMQPools<MqttPoolLazy> namedMQPools;
	MessageQueueFactoryImpl() {
		super(MHELPER);
		this.mqInstanceFactory = MqttFactory.MQ_INSTANCE_SUPPLIER;
		namedMQPools = MqttPoolLazys.NAMED_POOLS;
	}

	@Override
	protected NamedMQPools<MqttPoolLazy> getNamedMQPools() {
		return namedMQPools;
	}

	@Override
	protected MQInstanceSupplier<MqttPoolLazy> getMqInstanceFactory() {
		return mqInstanceFactory;
	}
}
