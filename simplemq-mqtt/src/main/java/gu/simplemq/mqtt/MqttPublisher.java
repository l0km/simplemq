package gu.simplemq.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import gu.simplemq.BaseMQPublisher;
import gu.simplemq.Constant;
import gu.simplemq.IAdvisor;
import gu.simplemq.IPublisher;
import gu.simplemq.MQConnectionException;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.ZeroAdvisor;

/**
 * 
 * {@link IPublisher} MQTT实现
 * @author guyadong
 *
 */
public class MqttPublisher extends BaseMQPublisher<MqttClient> implements Constant{
	private final int qos;
	private final IAdvisor adivsor = new ZeroAdvisor();
	public MqttPublisher(MqttPoolLazy poolLazy) {
		super(poolLazy);
		this.qos = poolLazy.getQos();
	}
	@Override
	protected void doSend(MqttClient c, String channel, Iterable<String> messages) throws Exception {
        for(String message : messages){
	        // 发布消息
        	try {
        		c.publish(channel, message.getBytes(UTF_8), qos, false);
			} catch (MqttException e) {
				if(e.getReasonCode() == MqttException.REASON_CODE_CLIENT_NOT_CONNECTED){
					throw new MQConnectionException(e);
				}
				throw new MQRuntimeException(e);
			} catch (Throwable e) {
				throw new MQRuntimeException(e);
			}
        }
	}
	@Override
	public IAdvisor getAdvisor() {
		return adivsor;
	}

}
