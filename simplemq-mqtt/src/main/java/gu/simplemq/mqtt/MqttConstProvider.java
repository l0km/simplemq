package gu.simplemq.mqtt;


import java.util.Map;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MessageQueueType;

public class MqttConstProvider implements MQConstProvider,MqttConstants{
	public static final MqttConstProvider MPROVIDER = new MqttConstProvider();
	private static final String[] optionalLocationNames = new String[]{MQTT_serverURI,MQTT_connOpts_serverURIs};
	private static final Map<String, String> NATIVE_SCHEME_MAP = ImmutableMap.of(
			MQTT_SCHEMA, PAHO_MQTT_SCHEMA,
			MQTT_SSL_SCHEMA, PAHO_MQTT_SSL_SCHEMA);
	/** 
	 * MQTT缺省连接参数<br>
	 * 这里没有使用guava的ImmutableMap，因为HashMap允许Value为null, ImmutableMap不允许 
	 **/
	@SuppressWarnings("serial")
	static final Properties DEFAULT_PARAMETERS = new Properties(){
		{
			put(MQTT_serverURI, DEFAULT_MQTT_URI);
			put(MQTT_connOpts_automaticReconnect, true);
		}
	};

	@Override
	public String getDefaultHost() {
		return DEFAULT_MQTT_HOST;
	}

	@Override
	public int getDefaultPort() {
		return DEFAULT_MQTT_PORT;
	}

	@Override
	public String getDefaultMQLocation() {
		return DEFAULT_MQTT_URI;
	}
	
	@Override
	public String getMainLocationName() {
		return MQTT_serverURI;
	}

	@Override
	public String getMainUserName() {
		return MQTT_connOpts_username;
	}

	@Override
	public String getMainPassword() {
		return MQTT_connOpts_password;
	}

	@Override
	public String getMainClientID() {
		return MQTT_clientId;
	}

	@Override
	public String getMainTimeout() {
		return MQTT_connOpts_connectionTimeout;
	}
	
	@Override
	public String getMainConnectTimeout() {
		return getMainTimeout();
	}

	@Override
	public String[] getOptionalLocationNames() {
		return optionalLocationNames;
	}
	@Override
	public Properties getDefaultMQProperties() {
		return DEFAULT_PARAMETERS;
	}
	@Override
	public Map<String, String> getNativeSchemeMap() {
		return NATIVE_SCHEME_MAP;
	}

	@Override
	public MessageQueueType getImplType() {
		return MessageQueueType.PAHO;
	}

	@Override
	public String getProtocol() {
		return MQTT_SCHEMA;
	}
}
