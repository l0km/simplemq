package gu.simplemq.proton;

import java.io.IOException;
import java.util.Date;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IProducer;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;

/**
 * @author guyadong
 *
 */
public class ProtonProducerTest implements ProtonConstants{

	@Test
	public void test2() throws InterruptedException, IOException {
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, AMQP_SCHEMA+"://user:password@" + DEFAULT_AMQP_HOST + ":"+DEFAULT_AMQP_PORT);
		IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.PROTON).init(m);
			IProducer producer = factory.getProducer();
			Channel<Date> datech = new Channel<Date>("datequeue",Date.class);
			for(int i=0; i<100; ){
				Date date = new Date();
				try{
					producer.produce(datech,date);
					++i;
					logger.info("" + i + ":" + date.getTime() +" : " +date.toString());
				}catch (Throwable e) {
					e.printStackTrace();
				}
				Thread.sleep(2000);
			}
	}
}
