package gu.simplemq.proton;

import java.util.Map;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MessageQueueType;

public class ProtonConstProvider implements MQConstProvider,ProtonConstants{
	public static final ProtonConstProvider APROVIDER = new ProtonConstProvider();
	private static final String[] optionalLocationNames = new String[]{JCON_remoteURI};
	private static final Map<String, String> NATIVE_SCHEME_MAP = ImmutableMap.of(
			AMQP_SCHEMA, AMQP_SCHEMA);
	/** 
	 * 缺省连接参数<br>
	 * 这里没有使用guava的ImmutableMap，因为HashMap允许Value为null, ImmutableMap不允许 
	 **/
	public static final Properties DEFAULT_PARAMETERS = new Properties(){
		private static final long serialVersionUID = 1L;
		{
			put(JCON_remoteURI, DEFAULT_AMQP_BROKER_URL);
		}
	};
	
	private ProtonConstProvider() {
		super();
	}

	@Override
	public String getDefaultHost() {
		return DEFAULT_AMQP_HOST;
	}

	@Override
	public int getDefaultPort() {
		return DEFAULT_AMQP_PORT;
	}

	@Override
	public String getDefaultMQLocation() {
		return DEFAULT_AMQP_BROKER_URL;
	}
	
	@Override
	public String getMainLocationName() {
		return JCON_remoteURI;
	}
	
	@Override
	public String getMainUserName() {
		return null;
	}

	@Override
	public String getMainPassword() {
		return null;
	}

	@Override
	public String getMainClientID() {
		return JCON_CLIENTID;
	}
	@Override
	public String getMainTimeout() {
		return JCON_sendTimeout;
	}
	
	@Override
	public String getMainConnectTimeout() {
		return JCON_connectTimeout;
	}
	@Override
	public String[] getOptionalLocationNames(){
		return optionalLocationNames;
	}
	@Override
	public Properties getDefaultMQProperties(){
		return DEFAULT_PARAMETERS;
	}

	@Override
	public Map<String, String> getNativeSchemeMap() {
		return NATIVE_SCHEME_MAP;
	}
	@Override
	public final MessageQueueType getImplType(){
		return MessageQueueType.PROTON;
	}

	@Override
	public String getProtocol() {
		return AMQP_SCHEMA;
	}
}
