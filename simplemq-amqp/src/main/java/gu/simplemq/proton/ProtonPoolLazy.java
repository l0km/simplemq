package gu.simplemq.proton;

import java.util.Properties;

import gu.simplemq.jms.JmsPoolLazy;

/**
 * @author guyadong
 * @since 2.4.0
 */
public class ProtonPoolLazy extends JmsPoolLazy {

	/**
	 * 构造方法，用于{@link gu.simplemq.pool.NamedMQPools#createPool(Properties)}反射调用创建实例
	 * 
	 * @param props
	 */
	public ProtonPoolLazy(Properties props) {
		super(props, MQContextImpl.PROTON_CONTEXT);
	}

	@Override
	public PooledConnectionFactory createPooledConnectionFactory(Properties properties) {
		PooledConnectionFactory pool = new PooledConnectionFactory();
		pool.setProperties(properties);
		return pool;
	}

}
