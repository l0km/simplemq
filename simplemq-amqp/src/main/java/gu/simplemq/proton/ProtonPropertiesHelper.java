package gu.simplemq.proton;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MQPropertiesHelper;

public class ProtonPropertiesHelper extends MQPropertiesHelper {
	static final ProtonPropertiesHelper PHELPER= new ProtonPropertiesHelper();
	ProtonPropertiesHelper() {
		super();
	}

	@Override
	public MQConstProvider getConstProvider() {
		return ProtonConstProvider.APROVIDER;
	}

}
