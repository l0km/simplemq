package gu.simplemq.proton;

import gu.simplemq.jms.JmsConstants;

public interface ProtonConstants extends JmsConstants{
	
	public static final String JCON_remoteURI = "remoteURI";
	public static final String JCON_username = MQ_USERNAME;
	public static final String JCON_password = MQ_PASSWORD;
	public static final String JCON_CLIENTID = MQ_CLIENTID;
	public static final String JCON_forceAsyncSend = "forceAsyncSend";
	public static final String JCON_forceSyncSend = "forceSyncSend";
	public static final String JCON_forceAsyncAcks = "forceAsyncAcks";
	public static final String JCON_localMessagePriority = "localMessagePriority";
	public static final String JCON_localMessageExpiry = "localMessageExpiry";
	public static final String JCON_receiveLocalOnly = "receiveLocalOnly";
	public static final String JCON_receiveNoWaitLocalOnly = "receiveNoWaitLocalOnly";
	public static final String JCON_populateJMSXUserID = "populateJMSXUserID";
	public static final String JCON_queuePrefix = "queuePrefix";
	public static final String JCON_topicPrefix = "topicPrefix";
	public static final String JCON_validatePropertyNames = "validatePropertyNames";
	public static final String JCON_sendTimeout = "sendTimeout";
	public static final String JCON_requestTimeout = "requestTimeout";
	public static final String JCON_closeTimeout = "closeTimeout";
	public static final String JCON_connectTimeout = "connectTimeout";
	public static final String JCON_clientIDPrefix = "clientIDPrefix";
	public static final String JCON_connectionIDPrefix = "connectionIDPrefix";
	
	public static final String DEFAULT_AMQP_HOST = "localhost";
	public static final String DEFAULT_AMQP_BROKER_URL = AMQP_SCHEMA + "://" + DEFAULT_AMQP_HOST + ":" + DEFAULT_AMQP_PORT;
}
