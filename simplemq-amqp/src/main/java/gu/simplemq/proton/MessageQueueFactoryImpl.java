package gu.simplemq.proton;

import gu.simplemq.BaseMessageQueueFactory;
import gu.simplemq.IMQContext;
import gu.simplemq.MQInstanceSupplier;
import gu.simplemq.jms.JmsPoolLazys;
import gu.simplemq.pool.NamedMQPools;

/**
 * @author guyadong
 * @since 2.4.0
 */
public class MessageQueueFactoryImpl extends BaseMessageQueueFactory<ProtonPoolLazy> {
	private final ProtonFactory mqInstanceFactory;
	private final NamedMQPools<ProtonPoolLazy> namedMQPools;

	MessageQueueFactoryImpl(IMQContext mqContext) {
		super(ProtonPropertiesHelper.PHELPER);
		this.mqInstanceFactory = new ProtonFactory();
		this.namedMQPools = new JmsPoolLazys<ProtonPoolLazy>(ProtonPropertiesHelper.PHELPER) {};
	}

	@Override
	protected NamedMQPools<ProtonPoolLazy> getNamedMQPools() {
		return namedMQPools;
	}

	@Override
	protected MQInstanceSupplier<ProtonPoolLazy> getMqInstanceFactory() {
		return mqInstanceFactory;
	}
}
