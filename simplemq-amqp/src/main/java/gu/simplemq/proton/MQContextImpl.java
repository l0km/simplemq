package gu.simplemq.proton;

import gu.simplemq.BaseMQContext;
import gu.simplemq.Constant;
import gu.simplemq.IMQContext;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MQPropertiesHelper;

public class MQContextImpl extends BaseMQContext implements IMQContext,Constant {

	static final MQContextImpl PROTON_CONTEXT = new MQContextImpl();
	private final ProtonPropertiesHelper protonHelper;
	private final MessageQueueFactoryImpl factory;

	public MQContextImpl() {
		super();
		protonHelper = ProtonPropertiesHelper.PHELPER;
		factory = new MessageQueueFactoryImpl(this);
	}

	@Override
	public MQPropertiesHelper getPropertiesHelper() {
		return protonHelper;
	}

	@Override
	public IMessageQueueFactory getMessageQueueFactory(){
		return factory;
	}
}
