package gu.simplemq.jms;

import javax.jms.Connection;
import javax.jms.JMSException;

import gu.simplemq.BaseMQPublisher;
import gu.simplemq.IAdvisor;
import gu.simplemq.IPublisher;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.ZeroAdvisor;
import gu.simplemq.jms.BaseSender.PublishSender;

/**
 * 
 * {@link IPublisher} ACTIVEMQ实现
 * @author guyadong
 *
 */
public class JmsPublisher extends BaseMQPublisher<Connection> implements AutoCloseable,JmsConstants{
	private final PublishSender sender;
	private final IAdvisor advisor = new ZeroAdvisor();
	public JmsPublisher(JmsPoolLazy poolLazy) {
		super(poolLazy);
		sender = new PublishSender();
	}
	@Override
	protected void doSend(Connection c, String channel, Iterable<String> messages) throws Exception {
		try {
			sender.doSend(c, channel, messages);		
		} catch (JMSException e) {
			throw new MQRuntimeException(e);
		}
	}
	@Override
	public IAdvisor getAdvisor() {
		return advisor;
	}
	@Override
	public void close()  {
	}
}
