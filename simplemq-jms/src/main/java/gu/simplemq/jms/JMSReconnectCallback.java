package gu.simplemq.jms;

/**
 * JMS 重连机制回调接口
 * @author guyadong
 * @since 2.3.8
 */
public interface JMSReconnectCallback{
	/**
	 * 连接异常侦听
	 * @throws Exception 
	 */
	public void onConnectionLost() throws Exception;
	/**
	 * 尝试重连动作
	 * @throws Exception 失败抛出异常 JMSException 或异常原因(cause)为JMSException 则继续异步执行重试
	 */
	public void tryReconnecting() throws Exception;
	/**
	 * 返回当前接口对象所属的模块名,用于日志输出
	 */
	public String ownerName();
}