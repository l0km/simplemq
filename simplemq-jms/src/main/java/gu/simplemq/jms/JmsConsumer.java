package gu.simplemq.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;
/**
 * JMS消费者模型实现(线程安全)<br>
 * @author guyadong
 *
 */
public class JmsConsumer extends BaseJmsDispatcher{
	JmsConsumer(JmsPoolLazy pool) {
		super(pool);
	}

	@Override
	protected Destination makeDestination(Session session, String name) throws JMSException {
		return session.createQueue(name);
	}
}
