package gu.simplemq.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * JMS 消息订阅实现(线程安全)<br>
 * 每个 {@link JmsPoolLazy} 实例对应保持一个 {@link JmsSubscriber} 对象<br>
 * 对象可以复用(反复打开关闭) <br>
 * @author guyadong
 *
 */
public class JmsSubscriber extends BaseJmsDispatcher {
	JmsSubscriber(JmsPoolLazy pool) {
		super(pool);
	}

	@Override
	protected Destination makeDestination(Session session, String name) throws JMSException {
		return session.createTopic(name);
	}
	
}
