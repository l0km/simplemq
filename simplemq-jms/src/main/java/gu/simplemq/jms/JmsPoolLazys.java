package gu.simplemq.jms;

import java.util.Map;
import java.util.Properties;
import javax.jms.Connection;
import gu.simplemq.Constant;
import gu.simplemq.MQPropertiesHelper;
import gu.simplemq.pool.BaseMQPool.MQPoolException;
import gu.simplemq.pool.NamedMQPools;


/**
 * 基于名字管理的 JMS连接池（线程安全）<br>
 * @author guyadong
 *
 */
public class JmsPoolLazys<P extends JmsPoolLazy> extends NamedMQPools<P> implements Constant{
	protected JmsPoolLazys(MQPropertiesHelper propertiesHelper) {
		super(propertiesHelper);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public void checkConnect(Map input,Long timeoutMills) throws MQPoolException{
		Properties props = propertiesHelper.initParameters(input);
		String connectTimeout = propertiesHelper.getConstProvider().getMainConnectTimeout();
		if(timeoutMills != null && timeoutMills > 0 && connectTimeout != null){
			props.setProperty(connectTimeout, timeoutMills.toString());
		}
		try (JmsPoolLazy pool = createPool(props)){
	    	@SuppressWarnings("unused")
			Connection connect = pool.apply();
//	    	connect.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
			pool.free();
		} catch (Exception e) {
			throw new MQPoolException(e);
		}
	}
}
