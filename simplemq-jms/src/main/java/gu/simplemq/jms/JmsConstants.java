package gu.simplemq.jms;

import gu.simplemq.Constant;

public interface JmsConstants extends Constant{
	public static final String APOOL_maximumActiveSessionPerConnection = "pool.maximumActiveSessionPerConnection";
	public static final String APOOL_maxConnections = "pool.maxConnections";
	public static final String APOOL_idleTimeout = "pool.idleTimeout";
	public static final String APOOL_expiryTimeout = "pool.expiryTimeout";
	public static final String APOOL_timeBetweenExpirationCheckMillis = "pool.timeBetweenExpirationCheckMillis";
	public static final String APOOL_createConnectionOnStartup = "pool.createConnectionOnStartup";
	public static final String APOOL_useAnonymousProducers = "pool.useAnonymousProducers";
	public static final String APOOL_blockIfSessionPoolIsFullTimeout = "pool.blockIfSessionPoolIsFullTimeout";
	public static final String APOOL_reconnectOnException = "pool.reconnectOnException";
	
	public static final String PROP_CONSUMER_COUNT = "consumerCount";
}
