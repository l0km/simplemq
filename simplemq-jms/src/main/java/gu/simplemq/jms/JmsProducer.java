package gu.simplemq.jms;

import javax.jms.Connection;
import javax.jms.JMSException;

import gu.simplemq.BaseMQProducer;
import gu.simplemq.IAdvisor;
import gu.simplemq.IProducer;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.ZeroAdvisor;
import gu.simplemq.jms.BaseSender.ProducerSender;
/**
 * {@link IProducer} ACTIVEMQ实现
 * @author guyadong
 *
 */
public class JmsProducer extends BaseMQProducer<Connection> implements AutoCloseable,JmsConstants{
	private IAdvisor adivsor = new ZeroAdvisor();
	private final ProducerSender sender;
	public JmsProducer(JmsPoolLazy poolLazy) {
		super(poolLazy);
		this.sender = new ProducerSender();
	}

	@Override
	public IAdvisor getAdvisor() {
		return adivsor;
	}

	@Override
	protected void doSend(Connection c, String channel, Iterable<String> messages) throws Exception {
		try{
			sender.doSend(c, channel, messages);		
		} catch (JMSException e) {
			throw new MQRuntimeException(e);
		}
	}

	@Override
	public void close()  {
	}

}
