package gu.simplemq.redis;

import gu.simplemq.BaseMQProducer;
import gu.simplemq.IAdvisor;
import gu.simplemq.MQConnectionException;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.pool.BaseMQPool;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisException;

public class RedisProducer2 extends BaseMQProducer<Jedis>implements RedisConstants {

	private final RedisConsumerAdvisor advisor;

	public RedisProducer2(BaseMQPool<Jedis> poolLazy) {
		super(poolLazy);
		this.advisor = RedisConsumerAdvisor.queueAdvisorOf(poolLazy);

	}

	@Override
	public IAdvisor getAdvisor() {
		return advisor;
	}

	@Override
	protected void doSend(Jedis c, String channel, Iterable<String> messages) throws Exception {
		for(String message : messages){
			try {
				c.rpush(channel, message);
			} catch (JedisConnectionException e) {
				throw new MQConnectionException(e);
			}catch (JedisException e) {
				throw new MQRuntimeException(e);
			}
		}
	}

}
