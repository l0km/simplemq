package gu.simplemq.redis;


import static redis.clients.jedis.Protocol.*;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;

import java.util.Properties;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MessageQueueType;
import gu.simplemq.redis.JedisPoolLazy.PropName;

public class RedisConstProvider implements MQConstProvider,RedisConstants{
	public static final RedisConstProvider RPROVIDER = new RedisConstProvider();
	private static final String[] optionalLocationNames = new String[]{};
	private static final Map<String, String> NATIVE_SCHEME_MAP = ImmutableMap.of(
			REDIS_SCHEMA, REDIS_SCHEMA);
	@Override
	public String getDefaultHost() {
		return DEFAULT_HOST;
	}

	@Override
	public int getDefaultPort() {
		return DEFAULT_PORT;
	}

	@Override
	public String getDefaultMQLocation() {
		return DEFAULT_REDIS_URI;
	}
	
	@Override
	public String getMainLocationName() {
		return MQ_URI;
	}

	@Override
	public String getMainUserName() {
		return null;
	}

	@Override
	public String getMainPassword() {
		return MQ_PASSWORD;
	}

	@Override
	public String getMainClientID() {
		return null;
	}

	@Override
	public String getMainTimeout() {
		return "timeout";
	}
	
	@Override
	public String getMainConnectTimeout() {
		return getMainTimeout();
	}

	@Override
	public String[] getOptionalLocationNames() {
		return optionalLocationNames;
	}

	public Properties getDefaultMQProperties() {
		Properties props = new Properties();
		for(Entry<PropName, Object> entry : JedisPoolLazy.DEFAULT_PARAMETERS.entrySet()){
			if (null != entry.getValue()) {
				props.put(entry.getKey().name(), entry.getValue());
			}
		}
		return props;
	}

	@Override
	public Map<String, String> getNativeSchemeMap() {
		return NATIVE_SCHEME_MAP;
	}

	@Override
	public MessageQueueType getImplType() {
		return MessageQueueType.REDIS;
	}

	@Override
	public String getProtocol() {
		return REDIS_SCHEMA;
	}

}
