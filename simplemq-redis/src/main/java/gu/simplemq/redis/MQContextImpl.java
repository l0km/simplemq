package gu.simplemq.redis;

import gu.simplemq.BaseMQContext;
import gu.simplemq.Constant;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MQPropertiesHelper;

public class MQContextImpl extends BaseMQContext implements Constant{
	private static final MessageQueueFactoryImpl factory = new MessageQueueFactoryImpl();

	public MQContextImpl() {
		super();
	}

	@Override
	public MQPropertiesHelper getPropertiesHelper() {
		return PropertiesHelper.RHELPER;
	}

	@Override
	public IMessageQueueFactory getMessageQueueFactory() {
		return factory;
	}

}
