package gu.simplemq.redis;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MQPropertiesHelper;

public class PropertiesHelper extends MQPropertiesHelper {
	public static final PropertiesHelper RHELPER = new PropertiesHelper();

	PropertiesHelper() {
	}

	@Override
	public MQConstProvider getConstProvider() {
		return RedisConstProvider.RPROVIDER;
	}
}
