package gu.simplemq.redis;

import gu.simplemq.MQInstanceFactory;

/**
 *  基于{@link MQInstanceFactory}实现 redis消息处理对象管理<br>
 * @author guyadong
 *
 */
public class RedisMQInstanceSupplier extends MQInstanceFactory<RedisConsumer, RedisProducer, RedisSubscriber, RedisPublisher, JedisPoolLazy> {
    public static final RedisMQInstanceSupplier MQ_INSTANCE_SUPPLIER = new RedisMQInstanceSupplier();
	RedisMQInstanceSupplier() {
	}

}
