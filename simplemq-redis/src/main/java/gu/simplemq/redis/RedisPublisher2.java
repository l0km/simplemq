package gu.simplemq.redis;

import com.google.common.base.Throwables;

import gu.simplemq.BaseMQPublisher;
import gu.simplemq.IAdvisor;
import gu.simplemq.MQConnectionException;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.pool.BaseMQPool;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisException;

public class RedisPublisher2 extends BaseMQPublisher<Jedis> implements RedisConstants {

	private final RedisConsumerAdvisor advisor;

	public RedisPublisher2(BaseMQPool<Jedis> poolLazy) {
		super(poolLazy);
		this.advisor = RedisConsumerAdvisor.topicAdvisorOf(poolLazy);
	}

	@Override
	protected void doSend(Jedis c, String channel, Iterable<String> messages) {
		for(String message : messages){
			try {
				c.publish(channel, message);				
			} catch (JedisConnectionException e) {
				throw new MQConnectionException(e);
			}catch (JedisException e) {
				throw new MQRuntimeException(e);
			}
		}
	}


	@Override
	public IAdvisor getAdvisor() {
		return advisor;
	}

}
