package gu.simplemq.redis;

import java.util.EnumMap;
import java.util.Map;
import gu.simplemq.pool.BaseMQPool.MQPoolException;
import gu.simplemq.pool.NamedMQPools;
import gu.simplemq.redis.JedisPoolLazy.PropName;
import redis.clients.jedis.exceptions.JedisException;

/**
 * @author guyadong
 * @since 2.4.0
 */
public class JedisPoolLazys extends NamedMQPools<JedisPoolLazy> {
	public static final JedisPoolLazys NAMED_POOLS = new JedisPoolLazys();
	protected JedisPoolLazys() {
		super(PropertiesHelper.RHELPER);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public void checkConnect(Map input, Long timeoutMills) throws MQPoolException {
		EnumMap<PropName, Object> props = propertiesHelper.initParameters(input).asEnumMap(PropName.class);
		if(timeoutMills != null && timeoutMills > 0){
			props.put(PropName.timeout, timeoutMills);
		}
		try {
			JedisUtils.checkConnect(props);
		} catch (JedisException e) {
			throw new MQPoolException(e);
		}
	}
}
