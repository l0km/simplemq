package gu.simplemq.redis;

import gu.simplemq.Constant;
import static redis.clients.jedis.Protocol.*;

public interface RedisConstants extends Constant{

	String CONSUMER_SET_SUFFIX = "_consumers";
	String CONSUMER_COUNTER = "simplemq_consumer_counter";
	String SUBSCRIBER_SET_SUFFIX = "_subscribers";
	String SUBSCRIBER_COUNTER = "simplemq_subscriber_counter";
	
	public static final String DEFAULT_REDIS_HOST = DEFAULT_HOST;
	public static final int DEFAULT_REDIS_PORT = DEFAULT_PORT;
	public static final String DEFAULT_REDIS_SCHEMA = "redis";
	
	public static final String DEFAULT_REDIS_URI = DEFAULT_REDIS_SCHEMA + "://" + DEFAULT_REDIS_HOST + ":" + DEFAULT_REDIS_PORT + "/" + DEFAULT_DATABASE;

}
