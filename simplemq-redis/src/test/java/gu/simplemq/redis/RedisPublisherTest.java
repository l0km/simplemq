package gu.simplemq.redis;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.simplemq.Channel;
import gu.simplemq.IPublisher;

/**
 * @author guyadong
 *
 */
public class RedisPublisherTest {
	private static final Logger logger = LoggerFactory.getLogger(RedisPublisherTest.class);
	@Before
	public void init() {
		JedisPoolLazys.NAMED_POOLS.defineDefaultPool(null);
	}
	@Test
	public void test() throws InterruptedException {
		 Channel<Date> chat1 = new Channel<Date>("chat1",Date.class);
		IPublisher publisher = RedisFactory.getPublisher(JedisPoolLazys.NAMED_POOLS.getDefaultPool());
		for(int i=0;i<100;++i){
			Date date = new Date();
			publisher.publish(chat1, date);
			logger.info(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
	}

}
