package gu.simplemq.redis;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.net.HostAndPort;

import gu.simplemq.Constant;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;

public class MessageQueueFactoryImplTest implements Constant {

	@Test
	public void test4GetHostAndPort() {
		ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI, "redis://192.168.10.226:3233/0");
		try(IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.REDIS).init(m)){
			HostAndPort hostAndPort = factory.getHostAndPort();
			logger.info("hostAndPort{}", hostAndPort);
			assertEquals(hostAndPort.getHost(), "192.168.10.226");
			assertEquals(hostAndPort.getPort(), 3233);
		} catch (Exception e) {
			e.printStackTrace();
		};
	}
}
