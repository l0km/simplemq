package gu.simplemq.redis;

import java.util.Date;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.simplemq.Channel;
import gu.simplemq.ConsumerSingle;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class RedisConsumerTest {
	private static final Logger logger = LoggerFactory.getLogger(RedisConsumerTest.class);
	@Before
	public void init() {
		JedisPoolLazys.NAMED_POOLS.defineDefaultPool(null);
	}
	/**
	 * 等待程序结束
	 */
	private static void waitquit(){
		System.out.println("PRESS 'CTRL-C' or 'quit' to exit");
		Scanner scaner = new Scanner(System.in);
		try{
			while (scaner.hasNextLine()) {
				String str = scaner.next();
				if("quit".equalsIgnoreCase(str)){
					return ;
				}
			}
		}finally{
			scaner.close();
		}
	}
	@Test
	public void test1() {
		@SuppressWarnings("resource")
		ConsumerSingle<String> consumer = new RedisConsumerSingle<String>(String.class,JedisPoolLazys.NAMED_POOLS.getDefaultPool(),"list1").setAdapter(new IMessageAdapter<String>(){
			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				System.out.println("list1:" + t);
			}});
		try{
			consumer.open();
		}finally{
			consumer.close();
		}
	}
	@Test
	public void test(){
		RedisConsumer consumer = RedisFactory.getConsumer(JedisPoolLazys.NAMED_POOLS.getDefaultPool());
		Channel<String> list1 = new Channel<String>("list1",String.class,new IMessageAdapter<String>(){

			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				logger.info("{}:{}","list1",t);
			}} );
		Channel<String> list2 = new Channel<String>("list2",String.class,new IMessageAdapter<String>(){

			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				logger.info("{}:{}","list2",t);
			}} );
		Channel<String> list3 = new Channel<String>("list3",String.class,new IMessageAdapter<String>(){

			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				logger.info("{}:{}","list3",t);
			}} );
		consumer.register(list1,list2);
		consumer.register(list3);
		consumer.unregister(list1);
	}
	@Test
	public void test3(){
		RedisConsumer consumer = RedisFactory.getConsumer(JedisPoolLazys.NAMED_POOLS.getDefaultPool());
		Channel<Date> datech = new Channel<Date>("datequeue",Date.class,new IMessageAdapter<Date>(){

			@Override
			public void onSubscribe(Date t) throws SmqUnsubscribeException {
				logger.info("consum {}:{}","datech",t);
			}} );
		consumer.register(datech);
		logger.info("register {}",datech);
		waitquit();
	}
}
