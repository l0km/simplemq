package gu.simplemq.redis;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MQProperties;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class RedisSubscriberTest implements RedisConstants {
	private static final Logger logger = LoggerFactory.getLogger(RedisSubscriberTest.class);
	@Before
	public void init() {
		JedisPoolLazys.NAMED_POOLS.defineDefaultPool(null);
	}
	/**
	 * 等待程序结束
	 */
	private static void waitquit(){
		System.out.println("PRESS 'CTRL-C' or 'quit' to exit");
		Scanner scaner = new Scanner(System.in);
		try{
			while (scaner.hasNextLine()) {
				String str = scaner.next();
				if("quit".equalsIgnoreCase(str)){
					return ;
				}
			}
		}finally{
			scaner.close();
		}
	}
	@Test
	public void test() {
		RedisSubscriber subscriber = RedisFactory.getSubscriber(JedisPoolLazys.NAMED_POOLS.getDefaultPool());
		Channel<Date> chat1 = new Channel<>("chat1",Date.class,new IMessageAdapter<Date>(){

			@Override
			public void onSubscribe(Date t) throws SmqUnsubscribeException {
				logger.info("{}:{}","chat1",t);
			}} );
		Channel<String> chat2 = new Channel<String>("chat2",String.class,new IMessageAdapter<String>(){

			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				logger.info("{}:{}","chat2",t);
			}} );
		Channel<String> chat3 = new Channel<String>("chat3",String.class,new IMessageAdapter<String>(){

			@Override
			public void onSubscribe(String t) throws SmqUnsubscribeException {
				logger.info("{}:{}","chat3",t);
			}} );
		subscriber.register(chat1,chat2);
		
		subscriber.register(chat3);
		waitquit();
//		subscriber.unsubscribe(chat1.name);
//		subscriber.unsubscribe();
	}

	@Test
	public void test3CheckConnect() {
		ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI, "jedis://localhost:6379/0");
		MQProperties props = PropertiesHelper.RHELPER.asMQProperties(m);
		boolean connectable = JedisPoolLazys.NAMED_POOLS.testConnect(props, null);
		logger.info("{} connectable {}", props.getLocation(), connectable);
		assertTrue(connectable);
	}
	@Test
	public void test4MessageQueueFactoryImpl() {
		ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI, "jedis://localhost:6379/0");
		try(IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.REDIS).init(m)){
			logger.info("hostAndPort{}", factory.getHostAndPort());
		} catch (Exception e) {
			e.printStackTrace();
		};
	}
}
