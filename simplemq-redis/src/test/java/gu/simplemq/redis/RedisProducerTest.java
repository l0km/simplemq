package gu.simplemq.redis;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import gu.simplemq.Channel;

/**
 * @author guyadong
 *
 */
public class RedisProducerTest {
	@Before
	public void init() {
		JedisPoolLazys.NAMED_POOLS.defineDefaultPool(null);
	}
	@Test
	public void test() throws InterruptedException {
		RedisProducerSingle<Date> producer = new RedisProducerSingle<Date>(Date.class,JedisPoolLazys.NAMED_POOLS.getDefaultPool(),"datelist");
		for(int i=0;i<100;++i){
			Date date = new Date();
			producer.produce(date);
			System.out.println(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
	}
	@Test
	public void test2() throws InterruptedException {
		RedisProducer producer = RedisFactory.getProducer(JedisPoolLazys.NAMED_POOLS.getDefaultPool());
		Channel<Date> datech = new Channel<Date>("datequeue",Date.class);
		for(int i=0;i<100;++i){
			Date date = new Date();
			producer.produce(datech,date);
			System.out.println(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
	}
}
