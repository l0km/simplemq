package gu.simplemq.redis;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
/**
 * @author guyadong
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SubTest {

	private JedisPoolLazy pool;
	private JedisPubSub jedisPubSub=new JedisPubSub(){
		@Override
		public void onMessage(String channel, String message) {
			System.out.println("@"+channel +":"+message);
		}};
	@Before
	public void init() {
		JedisPoolLazys.NAMED_POOLS.defineDefaultPool(null);
		pool = JedisPoolLazys.NAMED_POOLS.getDefaultPool();
	}

	@Test
	public void test1Subscribe() throws InterruptedException {
		System.out.println("jedisPubSub" + jedisPubSub.toString());
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				Jedis jedis = pool.apply();
				try {
					System.out.println("jedisPubSub" + jedisPubSub.toString());
					jedis.subscribe(jedisPubSub, "chat2");
				} finally {
					pool.free();
				}
			}
		});
		t.start();
		t.join();
		System.out.println("jedisPubSub" + jedisPubSub.toString());
		jedisPubSub.subscribe("chat2");

		jedisPubSub.unsubscribe();
	}
		
	

}
