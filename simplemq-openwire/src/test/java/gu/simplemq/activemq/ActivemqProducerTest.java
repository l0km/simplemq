package gu.simplemq.activemq;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IProducer;
import gu.simplemq.MessageQueueFactorys;

/**
 * @author guyadong
 *
 */
public class ActivemqProducerTest implements  ActivemqConstants{

	@Test
	public void test2() throws InterruptedException, IOException {
//		ImmutableMap<String, String> m = ImmutableMap.of(MQ_USERNAME, "user", MQ_PASSWORD, "password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, OPENWIRE_SCHEMA+"://user:password@" + DEFAULT_OPENWIRE_HOST + ":"+DEFAULT_OPENWIRE_PORT);
		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try {
			IProducer producer = factory.getProducer();
			Channel<Date> datech = new Channel<Date>("datequeue", Date.class);
			for (int i = 0; i < 100; ++i) {
				Date date = new Date();
				producer.produce(datech, date);
				logger.info(date.getTime() + " : " + date.toString());
				Thread.sleep(2000);
			}
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
