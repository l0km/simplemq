package gu.simplemq.activemq;

import static com.gitee.l0km.com4j.basex.bean.BeanPropertySupport.BEAN_SUPPORT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.ISubscriber;
import gu.simplemq.MQProperties;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.exceptions.SmqUnsubscribeException;
import gu.simplemq.jms.JmsPoolLazys;

/**
 * @author guyadong
 *
 */
public class ActivemqSubscriberTest implements ActivemqConstants{
	private static final String TOPIC_PREIFX = "ActiveMQ.Advisory.Consumer.Topic.";
	private static final String OPENWIRE_HOST = "localhost";

	private static ActiveMQConnectionFactory createFactory(){
		Properties props = new Properties();
    	props.setProperty("brokerURL","tcp://" + OPENWIRE_HOST + ":" + DEFAULT_OPENWIRE_PORT);
    	ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
       factory.setProperties(props);    
       return factory;
	}
	private static Connection conn;
	private static Session session;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ActiveMQConnectionFactory factory = createFactory();
		conn = factory.createConnection("user", "password");
		conn.setExceptionListener(new MyExceptionListener());
		conn.start();
		session = conn.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
		conn.close();
		//pool.close();
	}
	public void sub(Session session,String topic,MessageListener listener) throws JMSException {
		Topic activeMQTopic = session.createTopic(topic);
		MessageConsumer consumer = session.createConsumer(activeMQTopic);
		consumer.setMessageListener(listener);
	}
	private static void waitquit(){
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		try{
			while(!"quit".equalsIgnoreCase(reader.readLine())){
			}
			System.exit(0);
		} catch (IOException e) {
	
		}finally {
	
		}
	}
	@Test
	public void test1() {
		try {
			sub(session,TOPIC_PREIFX + "*", new AdvisoryListener());
			sub(session,"chat1", new LogListener());
			sub(session,"chat2", new LogListener());
			sub(session,"chat3", new LogListener());
			waitquit();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test2() {
//		ImmutableMap<String,String> m = ImmutableMap.of();
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, OPENWIRE_SCHEMA+"://user:password@" + OPENWIRE_HOST + ":"+DEFAULT_OPENWIRE_PORT);

		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try{
			ISubscriber subscriber = factory.getSubscriber();
			sub(session,TOPIC_PREIFX + "*",new AdvisoryListener());
			Channel<String> chat1 = new LogChannel("chat1");
			Channel<String> chat2 = new LogChannel("chat2");
			Channel<String> chat3 = new LogChannel("chat3");
			subscriber.register(chat1,chat2);
			subscriber.register(chat3);
			waitquit();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//		subscriber.unsubscribe(chat1.name);
		//		subscriber.unsubscribe();
	}
	/**
	 * 连接测试
	 */
	@Test
	public void test4CheckConnect() {
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, OPENWIRE_SCHEMA+"://user:password@" + OPENWIRE_HOST + ":"+DEFAULT_OPENWIRE_PORT);
		MQProperties props = ActivemqPropertiesHelper.AHELPER.asMQProperties(m);
		JmsPoolLazys<ActivemqPoolLazy> namedPools = new JmsPoolLazys<ActivemqPoolLazy>(ActivemqPropertiesHelper.AHELPER) {};
		boolean connectable = namedPools.testConnect(props, null);
		logger.info("{} connectable {}", props.getLocation(), connectable);
		assertTrue(connectable);
	}
	/**
	 * 定义连接池测试
	 */
	@Test
	public void test5DefinePool() {
		ImmutableMap<String, ?> m = ImmutableMap.<String, Object>of(MQ_URI,
				OPENWIRE_SCHEMA + "://user:password@" + OPENWIRE_SCHEMA + ":" + OPENWIRE_HOST, MQ_WS_URI,
				"ws://localhost:61614", MQ_PUBSUB_URI, "mqtt+ssl://localhost:1883");
		try (IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false)) {
			/** 在 props的基础上，增加额外的连接选项 */
			ImmutableMap<String, ?> other = ImmutableMap.<String, Object>builder()
					.putAll(m)
					/** 定义连接池时，指定额外的连接选项 */
					.put("connOpts.cleanSession", false).build();
			// 定义连接池
			factory.definePool("other", other);
			// 获取默认连接池的订阅者
			ISubscriber subscriber = factory.getSubscriber();
			testSubscriber(subscriber, true);
			// 获取指定连接池的订阅者
			ISubscriber otherSubscriber = factory.getSubscriber("other");
			logger.info("subscriber class {}", otherSubscriber.getClass().getName());
			// 两个订阅者对象不应该是同一个对象
			assertNotEquals(subscriber, otherSubscriber);
			assertTrue(otherSubscriber.getClass().getPackage().getName().startsWith("gu.simplemq.mqtt"));
			testSubscriber(otherSubscriber, false);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}
	}
	private void testSubscriber(ISubscriber subscriber, boolean exepctCleanSession) {
		logger.info("================");
		logger.info("subscriber {}",subscriber);
		logger.info("pool.canonicalURI {}",BEAN_SUPPORT.getProperty(subscriber, "pool.canonicalURI"));
		logger.info("pool.properties {}",BEAN_SUPPORT.getProperty(subscriber, "pool.properties"));
		logger.info("pool.connOpts.cleanSession {}",BEAN_SUPPORT.getProperty(subscriber, "pool.connOpts.cleanSession"));
		// 断言连接池的 cleanSession 是否与预期一致
		assertEquals(exepctCleanSession, BEAN_SUPPORT.getProperty(subscriber, "pool.connOpts.cleanSession"));
	}
	class LogChannel extends Channel<String>{

		protected LogChannel(String name) {
			super(name);
		}
		@Override
		public void onSubscribe(String t) throws SmqUnsubscribeException {
			logger.info(name + " msg:" + t);
		}
	}
	private static class AdvisoryListener implements MessageListener{
		@Override
		public void onMessage(Message message) {
			try {
				logger.info("dest {}",message.getJMSDestination());
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
	private static class LogListener implements MessageListener{
		private String textOf(Message message) throws JMSException{
			if(message instanceof TextMessage){
				return ((TextMessage) message).getText();
			}
			if(message instanceof BytesMessage){
				BytesMessage bytesMessage = (BytesMessage)message;
				byte[] buf = new byte[(int) bytesMessage.getBodyLength()];
				bytesMessage.readBytes(buf);
				return new String(buf);
			}
			throw new IllegalArgumentException(String.format("INVALID message type,%s,%s required",
					TextMessage.class.getName(),
					BytesMessage.class.getName()));
		}
		@Override
		public void onMessage(Message message) {
			try {
				logger.info("dest {}:{}",message.getJMSDestination(),textOf(message));
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
    private static class MyExceptionListener implements ExceptionListener {
        @Override
        public void onException(JMSException exception) {
            System.out.println("Connection ExceptionListener fired, exiting.");
            exception.printStackTrace(System.out);
            System.exit(1);
        }
    }
}
