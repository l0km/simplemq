package gu.simplemq.activemq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IConsumer;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class ActivemqConsumerTest implements ActivemqConstants {
	private static void waitquit() {
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			while (!"quit".equalsIgnoreCase(reader.readLine())) {
			}
			System.exit(0);
		} catch (IOException e) {

		} finally {

		}
	}

	@Test
	public void test1() throws IOException {
//		ImmutableMap<String, String> m = ImmutableMap.of(MQ_USERNAME, "user", MQ_PASSWORD, "password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, OPENWIRE_SCHEMA+"://user:password@" + DEFAULT_OPENWIRE_HOST + ":"+DEFAULT_OPENWIRE_PORT);
		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try {
			IConsumer consumer = factory.getConsumer();
			Channel<String> list1 = new Channel<String>("list1", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list1", t);
				}
			});
			Channel<String> list2 = new Channel<String>("list2", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list2", t);
				}
			});
			Channel<String> list3 = new Channel<String>("list3", String.class, new IMessageAdapter<String>() {

				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}", "list3", t);
				}
			});
			consumer.register(list1, list2);
			consumer.register(list3);
			waitquit();
			consumer.unregister(list1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test3() {
//		ImmutableMap<String, String> m = ImmutableMap.of(MQ_USERNAME, "user", MQ_PASSWORD, "password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, OPENWIRE_SCHEMA+"://user:password@" + DEFAULT_OPENWIRE_HOST + ":"+DEFAULT_OPENWIRE_PORT);
		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m, false);
		try {
			IConsumer consumer = factory.getConsumer();
			Channel<Date> datech = new Channel<Date>("datequeue", Date.class, new IMessageAdapter<Date>() {

				@Override
				public void onSubscribe(Date t) throws SmqUnsubscribeException {
					logger.info("consum {}:{}", "datech", t);
				}
			});
			consumer.register(datech);
			logger.info("register {}", datech);
			waitquit();
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
