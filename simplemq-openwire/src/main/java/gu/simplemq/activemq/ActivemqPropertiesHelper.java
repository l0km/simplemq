package gu.simplemq.activemq;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MQPropertiesHelper;

public class ActivemqPropertiesHelper extends MQPropertiesHelper {
	public static final ActivemqPropertiesHelper AHELPER = new ActivemqPropertiesHelper();
	ActivemqPropertiesHelper() {
		super();
	}

	@Override
	public MQConstProvider getConstProvider() {
		return ActivemqConstProvider.APROVIDER;
	}

}
