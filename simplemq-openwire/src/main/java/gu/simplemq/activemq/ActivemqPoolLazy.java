package gu.simplemq.activemq;

import java.util.Properties;

import org.apache.activemq.pool.PooledConnectionFactory;

import gu.simplemq.jms.JmsPoolLazy;

/**
 * @author guyadong
 * @since 2.4.0
 */
public class ActivemqPoolLazy extends JmsPoolLazy {

	/**
	 * 构造方法，用于{@link gu.simplemq.pool.NamedMQPools#createPool(Properties)}反射调用创建实例
	 * 
	 * @param props
	 */
	public ActivemqPoolLazy(Properties props) {
		super(props, MQContextImpl.ACTIVEMQ_CONTEXT);
	}

	@Override
	public PooledConnectionFactory createPooledConnectionFactory(Properties properties) {
		PooledConnectionFactory pool = new PooledConnectionFactory();
		pool.setProperties(properties);
		return pool;
	}

}
