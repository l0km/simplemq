package gu.simplemq.activemq;

import gu.simplemq.BaseMQContext;
import gu.simplemq.Constant;
import gu.simplemq.IMQContext;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MQPropertiesHelper;

public class MQContextImpl extends BaseMQContext implements IMQContext,Constant {

	static final MQContextImpl ACTIVEMQ_CONTEXT = new MQContextImpl();
	private final ActivemqPropertiesHelper protonHelper;
	private final MessageQueueFactoryImpl factory;

	public MQContextImpl() {
		super();
		protonHelper = ActivemqPropertiesHelper.AHELPER;
		factory = new MessageQueueFactoryImpl(this);
	}

	@Override
	public MQPropertiesHelper getPropertiesHelper() {
		return protonHelper;
	}

	@Override
	public IMessageQueueFactory getMessageQueueFactory(){
		return factory;
	}
}
