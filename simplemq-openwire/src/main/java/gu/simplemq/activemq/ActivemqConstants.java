package gu.simplemq.activemq;

import gu.simplemq.jms.JmsConstants;

public interface ActivemqConstants extends JmsConstants{
	
	public static final String ACON_DISPATCHASYNC = "dispatchAsync";
	public static final String ACON_BROKER_URL= "brokerURL";
	public static final String ACON_CLIENTID = MQ_CLIENTID;
	public static final String ACON_copyMessageOnSend = "copyMessageOnSend";
	public static final String ACON_disableTimeStampsByDefault = "disableTimeStampsByDefault";
	public static final String ACON_objectMessageSerializationDefered = "objectMessageSerializationDefered";
	public static final String ACON_optimizedMessageDispatch = "optimizedMessageDispatch";
	
	public static final String ACON_PASSWORD = MQ_PASSWORD;
	
	public static final String ACON_useAsyncSend = "useAsyncSend";
	public static final String ACON_useCompression = "useCompression";
	public static final String ACON_useRetroactiveConsumer = "useRetroactiveConsumer";
	public static final String ACON_watchTopicAdvisories = "watchTopicAdvisories";

	public static final String ACON_USERNAME ="userName";
	
	public static final String ACON_closeTimeout = "closeTimeout";
	public static final String ACON_alwaysSessionAsync = "alwaysSessionAsync";
	public static final String ACON_optimizeAcknowledge = "optimizeAcknowledge";
	public static final String ACON_statsEnabled = "statsEnabled";
	public static final String ACON_alwaysSyncSend = "alwaysSyncSend";
	public static final String ACON_producerWindowSize = "producerWindowSize";
	public static final String ACON_sendTimeout = "sendTimeout";
	public static final String ACON_connectResponseTimeout = "connectResponseTimeout";
	public static final String ACON_sendAcksAsync = "sendAcksAsync";
	public static final String ACON_auditDepth = "auditDepth";
	public static final String ACON_auditMaximumProducerNumber = "auditMaximumProducerNumber";
	public static final String ACON_checkForDuplicates = "checkForDuplicates";
	public static final String ACON_messagePrioritySupported = "messagePrioritySupported";
	public static final String ACON_transactedIndividualAck = "transactedIndividualAck";
	public static final String ACON_nonBlockingRedelivery = "nonBlockingRedelivery";
	public static final String ACON_maxThreadPoolSize = "maxThreadPoolSize";
	public static final String ACON_nestedMapAndListEnabled = "nestedMapAndListEnabled";
	public static final String ACON_consumerFailoverRedeliveryWaitPeriod = "consumerFailoverRedeliveryWaitPeriod";
	public static final String ACON_rmIdFromConnectionId = "rmIdFromConnectionId";
	public static final String ACON_consumerExpiryCheckEnabled = "consumerExpiryCheckEnabled";

	public static final String APP_consumerExpiryCheckEnabled = "prefetchPolicy.consumerExpiryCheckEnabled";
	public static final String APP_queueBrowserPrefetch = "prefetchPolicy.queueBrowserPrefetch";
	public static final String APP_topicPrefetch = "prefetchPolicy.topicPrefetch";
	public static final String APP_durableTopicPrefetch = "prefetchPolicy.durableTopicPrefetch";
	public static final String APP_optimizeDurableTopicPrefetch = "prefetchPolicy.optimizeDurableTopicPrefetch";
	public static final String APP_maximumPendingMessageLimit = "prefetchPolicy.maximumPendingMessageLimit";

	public static final String ARP_backOffMultiplier = "redeliveryPolicy.backOffMultiplier";
	public static final String ARP_collisionAvoidancePercent = "redeliveryPolicy.collisionAvoidancePercent";
	public static final String ARP_initialRedeliveryDelay = "redeliveryPolicy.initialRedeliveryDelay";
	public static final String ARP_maximumRedeliveryDelay = "redeliveryPolicy.maximumRedeliveryDelay";
	public static final String ARP_maximumRedeliveries = "redeliveryPolicy.maximumRedeliveries";
	public static final String ARP_useCollisionAvoidance = "redeliveryPolicy.useCollisionAvoidance";
	public static final String ARP_useExponentialBackOff = "redeliveryPolicy.useExponentialBackOff";
	public static final String ARP_redeliveryDelay = "redeliveryPolicy.redeliveryDelay";

	public static final String ABP_uploadUrl = "blobTransferPolicy.uploadUrl";
	public static final String ABP_brokerUploadUrl = "blobTransferPolicy.brokerUploadUrl";
	public static final String ABP_defaultUploadUrl = "blobTransferPolicy.defaultUploadUrl";
	public static final String ABP_uploadStrategy = "blobTransferPolicy.uploadStrategy";
	public static final String ABP_bufferSize = "blobTransferPolicy.bufferSize";
	
	public static final String DEFAULT_OPENWIRE_HOST = "localhost";
	public static final int DEFAULT_OPENWIRE_PORT = 61616;
	public static final String ACTIVEMQ_OPENWIRE_SCHEMA = "tcp";
	public static final String DEFAULT_OPENWIRE_BROKER_URL = OPENWIRE_SCHEMA + "://" + DEFAULT_OPENWIRE_HOST + ":" + DEFAULT_OPENWIRE_PORT;
}
