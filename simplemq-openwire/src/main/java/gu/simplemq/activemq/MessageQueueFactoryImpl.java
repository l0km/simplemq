package gu.simplemq.activemq;

import gu.simplemq.BaseMessageQueueFactory;
import gu.simplemq.IMQContext;
import gu.simplemq.MQInstanceSupplier;
import gu.simplemq.jms.JmsPoolLazys;
import gu.simplemq.pool.NamedMQPools;

/**
 * @author guyadong
 * @since 2.4.0
 */
public class MessageQueueFactoryImpl extends BaseMessageQueueFactory<ActivemqPoolLazy>{
	private final ActivemqFactory mqInstanceFactory;
	private final NamedMQPools<ActivemqPoolLazy> namedMQPools;

	MessageQueueFactoryImpl(IMQContext mqContext) {
		super(ActivemqPropertiesHelper.AHELPER);
		this.mqInstanceFactory = new ActivemqFactory();
		this.namedMQPools = new JmsPoolLazys<ActivemqPoolLazy>(ActivemqPropertiesHelper.AHELPER) {};
	}

	@Override
	protected NamedMQPools<ActivemqPoolLazy> getNamedMQPools() {
		return namedMQPools;
	}

	@Override
	protected MQInstanceSupplier<ActivemqPoolLazy> getMqInstanceFactory() {
		return mqInstanceFactory;
	}
}
