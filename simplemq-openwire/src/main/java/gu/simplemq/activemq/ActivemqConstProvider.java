package gu.simplemq.activemq;

import static javax.naming.Context.PROVIDER_URL;

import java.util.Map;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MessageQueueType;

public class ActivemqConstProvider implements MQConstProvider,ActivemqConstants{
	public static final ActivemqConstProvider APROVIDER = new ActivemqConstProvider();
	private static final String[] optionalLocationNames = new String[]{PROVIDER_URL,ACON_BROKER_URL};
	private static final Map<String, String> NATIVE_SCHEME_MAP = ImmutableMap.of(
			OPENWIRE_SCHEMA, ACTIVEMQ_OPENWIRE_SCHEMA);
	/** ACTIVEMQ缺省连接参数 **/
	static final Properties DEFAULT_PARAMETERS = new Properties(){
		private static final long serialVersionUID = 1L;
		{
			put(ACON_BROKER_URL, DEFAULT_OPENWIRE_BROKER_URL);
		}
	};
	
	private ActivemqConstProvider() {
		super();
	}

	@Override
	public String getDefaultHost() {
		return DEFAULT_OPENWIRE_HOST;
	}

	@Override
	public int getDefaultPort() {
		return DEFAULT_OPENWIRE_PORT;
	}

	@Override
	public String getDefaultMQLocation() {
		return DEFAULT_OPENWIRE_BROKER_URL;
	}
	
	@Override
	public String getMainLocationName() {
		return ACON_BROKER_URL;
	}
	
	@Override
	public String getMainUserName() {
		return ACON_USERNAME;
	}

	@Override
	public String getMainPassword() {
		return ACON_PASSWORD;
	}

	@Override
	public String getMainClientID() {
		return ACON_CLIENTID;
	}
	@Override
	public String getMainTimeout() {
		return ACON_sendTimeout;
	}
	@Override
	public String getMainConnectTimeout() {
		return ACON_connectResponseTimeout;
	}
	@Override
	public String[] getOptionalLocationNames(){
		return optionalLocationNames;
	}
	@Override
	public Properties getDefaultMQProperties(){
		Properties properties = new Properties();
		properties.putAll(DEFAULT_PARAMETERS);
		return properties;
	}

	@Override
	public Map<String, String> getNativeSchemeMap() {
		return NATIVE_SCHEME_MAP;
	}

	@Override
	public MessageQueueType getImplType() {
		return MessageQueueType.ACTIVEMQ;
	}

	@Override
	public String getProtocol() {
		return OPENWIRE_SCHEMA;
	}

}
