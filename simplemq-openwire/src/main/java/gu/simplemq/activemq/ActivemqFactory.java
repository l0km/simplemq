package gu.simplemq.activemq;

import gu.simplemq.MQInstanceFactory;
import gu.simplemq.jms.JmsConsumer;
import gu.simplemq.jms.JmsProducer;
import gu.simplemq.jms.JmsPublisher;
import gu.simplemq.jms.JmsSubscriber;

/**
 * @author guyadong
 * @since 2.4.0
 */
class ActivemqFactory extends MQInstanceFactory<JmsConsumer, JmsProducer, JmsSubscriber, JmsPublisher, ActivemqPoolLazy>{
	ActivemqFactory() {
	}
}