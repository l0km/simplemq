package gu.simplemq;

import java.util.Map;

import com.google.common.net.HostAndPort;

/**
 * 提供消息系统连接参数的接口
 * @author guyadong
 *
 */
public interface IMQConnParameterSupplier {
	/**
	 * 返回消息系统的实现类型(such as REDIS,ACTIVEMQ)
	 * @return 消息系统的实现类型
	 */
	MessageQueueType getImplType();
	/**
	 * @return 返回消息系统服务器的主机名和端口
	 */
	HostAndPort getHostAndPort();

	/**
	 * @return 返回消息系统连接参数,NAME-VALUE值对
	 */
	Map<String, Object> getMQConnParameters();
}
