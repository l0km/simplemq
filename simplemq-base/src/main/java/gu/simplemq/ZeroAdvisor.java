package gu.simplemq;

/**
 * @author guyadong
 * @since 2.3.9
 *
 */
public class ZeroAdvisor implements IAdvisor {

	public ZeroAdvisor() {
	}

	@Override
	public int consumerCountOf(String channelName) {
		return 0;
	}

	@Override
	public int subscriberCountOf(String channelName) {
		return 0;
	}

}
