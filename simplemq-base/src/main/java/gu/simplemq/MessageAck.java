package gu.simplemq;

import gu.simplemq.exceptions.SmqRuntimeException;

/**
 * 客户端消息确认(ACK)接口
 * @author guyadong
 * @since 2.3.9
 */
public interface MessageAck {
	public void acknowledge() throws SmqRuntimeException;
}
