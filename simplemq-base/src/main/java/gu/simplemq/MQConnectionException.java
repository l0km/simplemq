package gu.simplemq;

/**
 * 消息系统连接异常
 * @author guyadong
 *
 */
public class MQConnectionException extends MQRuntimeException {

	private static final long serialVersionUID = 1L;

	public MQConnectionException() {
	}

	public MQConnectionException(String message) {
		super(message);
	}

	public MQConnectionException(Throwable cause) {
		super(cause);
	}

	public MQConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
