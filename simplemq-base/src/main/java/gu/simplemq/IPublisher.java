package gu.simplemq;

import java.util.Collection;

import com.google.common.base.Function;

/**
 * 发布订阅接口
 * @author guyadong
 *
 */
public interface IPublisher{
	/**
	 * 向指定的频道发布消息
	 * @param channel 频道
	 * @param object 消息对象
	 * @return  the number of clients that received the message
	 */
	<T> long publish(Channel<T> channel,T object);

	/**
	 * 参见 {@link #publish(Channel, Object)}
	 * @param channel
	 * @param objects
	 */
	<T> void publish(Channel<T> channel, Collection<T> objects);

	/**
	 * 参见 {@link #publish(Channel, Object)}
	 * @param channel
	 * @param objects
	 */
	<T> void publish(Channel<T> channel, @SuppressWarnings("unchecked") T... objects);

	IAdvisor getAdvisor();

	/**
	 * 指定发送数据的字符串序列化实例，默认序列化为JSON字符串,对当前实例有效
	 * @param stringSerializer 为{@code null}时恢复使用默认序列化实例
	 * @return 当前对象
	 * @since 2.3.11
	 */
	IPublisher withStringSerializer(Function<Object, String> stringSerializer);
}
