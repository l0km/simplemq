package gu.simplemq;

public abstract class BaseMQContext implements IMQContext {

	protected BaseMQContext() {
	}

	@Override
	public final MessageQueueType getMessageQueueType() {
		return getPropertiesHelper().getConstProvider().getImplType();
	}

	@Override
	public final String getClientImplType(){
		return getPropertiesHelper().getConstProvider().getProtocol();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getClientImplType() == null) ? 0 : getClientImplType().hashCode());
		result = prime * result + ((getMessageQueueType() == null) ? 0 : getMessageQueueType().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BaseMQContext))
			return false;
		BaseMQContext other = (BaseMQContext) obj;
		if (getClientImplType() == null) {
			if (other.getClientImplType() != null)
				return false;
		} else if (!getClientImplType().equals(other.getClientImplType()))
			return false;
		if (getMessageQueueType() != other.getMessageQueueType())
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getName() + "@" + Integer.toHexString(hashCode())+" [mqType=");
		builder.append(getMessageQueueType());
		builder.append(", clientImplType=");
		builder.append(getClientImplType());
		builder.append("]");
		return builder.toString();
	}
	
}
