package gu.simplemq;

/**
 * 消息系统运行时异常
 * @author guyadong
 *
 */
public class MQRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MQRuntimeException() {
	}

	public MQRuntimeException(String message) {
		super(message);
	}

	public MQRuntimeException(Throwable cause) {
		super(cause);
	}

	public MQRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
