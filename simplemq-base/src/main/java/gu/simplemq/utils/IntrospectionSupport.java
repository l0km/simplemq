/**
 * copy from https://github.com/apache/activemq/blob/master/activemq-client/src/main/java/org/apache/activemq/util/IntrospectionSupport.java
 */
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package gu.simplemq.utils;

import static gu.simplemq.utils.TypeConversionSupport.convert;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.SSLServerSocket;


public final class IntrospectionSupport {

    private IntrospectionSupport() {
    }

    @SuppressWarnings({ "rawtypes" })
	public static boolean getProperties(Object target, Map props, String optionPrefix) {
        return setProperties(target, props,optionPrefix,true);
    }

    /**
     * @since 2.4.0
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static boolean setProperties(Object target, Map props, String optionPrefix, boolean removeIfSet) {
		boolean rc = false;
		if (target == null) {
			throw new IllegalArgumentException("target was null.");
		}
		if (props == null) {
			throw new IllegalArgumentException("props was null.");
		}
		if (null == optionPrefix) {
			optionPrefix = "";
		}
		for (Iterator<Entry> itor = props.entrySet().iterator(); itor.hasNext();) {
			Entry entry = itor.next();
			if (entry.getKey() instanceof String) {
				String name = (String) entry.getKey();
				if (!optionPrefix.isEmpty()) {
					if (name.startsWith(optionPrefix)) {
						name = name.substring(optionPrefix.length());
					} else {
						continue;
					}
				}
				if (setProperty(target, name, entry.getValue())) {
					if (removeIfSet) {
						itor.remove();
					}
					rc = true;
				}
			}
		}
		return rc;
	}
    @SuppressWarnings("rawtypes")
    public static boolean setProperties(Object target, Map props, String optionPrefix) {
    	return setProperties(target, props, optionPrefix, false);
    }

    @SuppressWarnings("rawtypes")
	public static Map<String, Object> extractProperties(Map props, String optionPrefix) {
        if (props == null) {
            throw new IllegalArgumentException("props was null.");
        }

        HashMap<String, Object> rc = new HashMap<String, Object>(props.size());

        for (Iterator<?> iter = props.keySet().iterator(); iter.hasNext();) {
            String name = (String)iter.next();
            if (name.startsWith(optionPrefix)) {
                Object value = props.get(name);
                name = name.substring(optionPrefix.length());
                rc.put(name, value);
                iter.remove();
            }
        }

        return rc;
    }

    @SuppressWarnings({ "rawtypes" })
	public static boolean setProperties(Object target, Map props) {
        return setProperties(target, props, true);
    }

    @SuppressWarnings({ "rawtypes" })
	public static boolean setProperties(Object target, Map props, boolean removeIfSet) {
        return setProperties(target, props, null, removeIfSet);
    }

    public static boolean setProperty(Object target, String name, Object value) {
        try {
            Class<?> clazz = target.getClass();
            if (target instanceof SSLServerSocket) {
                // overcome illegal access issues with internal implementation class
                clazz = SSLServerSocket.class;
            }
            Method setter = findSetterMethod(clazz, name);
            if (setter == null) {
                return false;
            }

            // JDK 11: class or setter might not be publicly accessible
            setter.setAccessible(true);

            // If the type is null or it matches the needed type, just use the
            // value directly
            if (value == null || value.getClass() == setter.getParameterTypes()[0]) {
                setter.invoke(target, value);
            } else {
                // We need to convert it
                setter.invoke(target, convert(value, setter.getParameterTypes()[0]));
            }
            return true;
        } catch (Throwable e) {
        	System.out.printf("Could not set property %s on %s\n", name, target);
        	e.printStackTrace();
            return false;
        }
    }

    public static Method findSetterMethod(Class<?> clazz, String name) {
        // Build the method name.
        name = "set" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            Class<?> params[] = method.getParameterTypes();
            if (method.getName().equals(name) && params.length == 1 ) {
                return method;
            }
        }
        return null;
    }

    public static Method findGetterMethod(Class<?> clazz, String name) {
        // Build the method name.
        name = "get" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            Class<?> params[] = method.getParameterTypes();
            if (method.getName().equals(name) && params.length == 0 ) {
                return method;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
	public static <T>T getProperty(Object target, String name,Class<T> targetClass) {
    	Class<?> clazz = target.getClass();
        try {
            if (target instanceof SSLServerSocket) {
                // overcome illegal access issues with internal implementation class
                clazz = (Class<T>) SSLServerSocket.class;
            }
            Method getter = findGetterMethod(clazz, name);
            if (getter == null) {
            	try {
            		Field field = clazz.getDeclaredField(name);	
            		field.setAccessible(true);
            		return convert(field.get(target), targetClass);
				} catch (NoSuchFieldException e) {
					return null;
				}            	
            }

            // JDK 11: class or setter might not be publicly accessible
            getter.setAccessible(true);

            // We need to convert it
            Object value = getter.invoke(target);
            return convert(value, targetClass);
        } catch (Throwable e) {
        	System.out.printf("Could not get property %s on %s\n", name, target);
        	e.printStackTrace();
        	return null;
        }
    }
    public static String toString(Object target) {
        return toString(target, Object.class, null);
    }

    public static String toString(Object target, Class<Object> stopClass) {
        return toString(target, stopClass, null);
    }

    public static String toString(Object target, Class<Object> stopClass, Map<String, Object> overrideFields) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
        addFields(target, target.getClass(), stopClass, map);
        if (overrideFields != null) {
            for(String key : overrideFields.keySet()) {
                Object value = overrideFields.get(key);
                map.put(key, value);
            }

        }
        StringBuffer buffer = new StringBuffer(simpleName(target.getClass()));
        buffer.append(" {");
        Set<Entry<String, Object>> entrySet = map.entrySet();
        boolean first = true;
        for (Map.Entry<String,Object> entry : entrySet) {
            Object value = entry.getValue();
            Object key = entry.getKey();
            if (first) {
                first = false;
            } else {
                buffer.append(", ");
            }
            buffer.append(key);
            buffer.append(" = ");

            appendToString(buffer, key, value);
        }
        buffer.append("}");
        return buffer.toString();
    }

    protected static void appendToString(StringBuffer buffer, Object key, Object value) {
        if (key.toString().toLowerCase(Locale.ENGLISH).contains("password")){
            buffer.append("*****");
        } else {
            buffer.append(value);
        }
    }

    public static String simpleName(Class<?> clazz) {
        String name = clazz.getName();
        int p = name.lastIndexOf(".");
        if (p >= 0) {
            name = name.substring(p + 1);
        }
        return name;
    }

    private static void addFields(Object target, Class<?> startClass, Class<Object> stopClass, LinkedHashMap<String, Object> map) {

        if (startClass != stopClass) {
            addFields(target, startClass.getSuperclass(), stopClass, map);
        }

        Field[] fields = startClass.getDeclaredFields();
        for (Field field : fields) {
            if (Modifier.isStatic(field.getModifiers()) || Modifier.isTransient(field.getModifiers())
                || Modifier.isPrivate(field.getModifiers())) {
                continue;
            }

            try {
                field.setAccessible(true);
                Object o = field.get(target);
                if (o != null && o.getClass().isArray()) {
                    try {
                        o = Arrays.asList((Object[])o);
                    } catch (Throwable e) {
                    }
                }
                map.put(field.getName(), o);
            } catch (Throwable e) {
                System.out.println("Error getting field " + field + " on class " + startClass + ". This exception is ignored.");
                e.printStackTrace();
            }
        }
    }
}