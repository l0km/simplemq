package gu.simplemq.utils;

import java.util.ServiceLoader;

/**
 * @author guyadong
 * @since 2.3.12
 *
 */
public class SPIUtils {

	private SPIUtils() {
	}
	/**
	 * 优先调用{@link ServiceLoader#load(Class)},如果返回为空则调用{@link ServiceLoader#load(Class, ClassLoader)}
	 * @param <T>
	 * @param service
	 */
	public static <T> ServiceLoader<T> serviceLoaderOf(Class<T> service){
		ServiceLoader<T> loader = ServiceLoader.load(service);
		return loader.iterator().hasNext() ? loader : ServiceLoader.load(service,service.getClassLoader());
	}
}
