package gu.simplemq.utils;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.StringTokenizer;

import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;

/**
 * Type conversion support for ActiveMQ.
 */
public final class TypeConversionSupport {

    private static final Converter IDENTITY_CONVERTER = new Converter() {
        @Override
        public Object convert(Object value) {
            return value;
        }
    };

    private static class ConversionKey {
        final Class<?> from;
        final Class<?> to;
        final int hashCode;

        public ConversionKey(Class<?> from, Class<?> to) {
            this.from = from;
            this.to = to;
            this.hashCode = from.hashCode() ^ (to.hashCode() << 1);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ConversionKey other = (ConversionKey) obj;
            if (from == null) {
                if (other.from != null)
                    return false;
            } else if (!from.equals(other.from))
                return false;
            if (to == null) {
                if (other.to != null)
                    return false;
            } else if (!to.equals(other.to))
                return false;
            return true;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    public interface Converter {
        Object convert(Object value);
    }

    private static final Map<ConversionKey, Converter> CONVERSION_MAP = new HashMap<ConversionKey, Converter>();
    static {
        Converter toStringConverter = new Converter() {
            @Override
            public Object convert(Object value) {
                return value.toString();
            }
        };
        CONVERSION_MAP.put(new ConversionKey(Boolean.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Byte.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Short.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Integer.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Long.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Float.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(Double.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(URI.class, String.class), toStringConverter);
        CONVERSION_MAP.put(new ConversionKey(BigInteger.class, String.class), toStringConverter);

        CONVERSION_MAP.put(new ConversionKey(String.class, Boolean.class), new Converter() {
            @Override
            public Object convert(Object value) {
            	return (value == null) 
            			? false 
            			: ((String)value).toLowerCase().matches("true|yes|on");
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Byte.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Byte.valueOf((String)value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Short.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Short.valueOf((String)value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Integer.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Integer.valueOf((String)value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Long.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Long.valueOf((String)value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Float.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Float.valueOf((String)value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, Double.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Double.valueOf((String)value);
            }
        });

        Converter longConverter = new Converter() {
            @Override
            public Object convert(Object value) {
                return Long.valueOf(((Number)value).longValue());
            }
        };
        CONVERSION_MAP.put(new ConversionKey(Byte.class, Long.class), longConverter);
        CONVERSION_MAP.put(new ConversionKey(Short.class, Long.class), longConverter);
        CONVERSION_MAP.put(new ConversionKey(Integer.class, Long.class), longConverter);
        CONVERSION_MAP.put(new ConversionKey(Date.class, Long.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Long.valueOf(((Date)value).getTime());
            }
        });

        Converter intConverter = new Converter() {
            @Override
            public Object convert(Object value) {
                return Integer.valueOf(((Number)value).intValue());
            }
        };
        CONVERSION_MAP.put(new ConversionKey(Byte.class, Integer.class), intConverter);
        CONVERSION_MAP.put(new ConversionKey(Short.class, Integer.class), intConverter);

        CONVERSION_MAP.put(new ConversionKey(Byte.class, Short.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return Short.valueOf(((Number)value).shortValue());
            }
        });

        CONVERSION_MAP.put(new ConversionKey(Float.class, Double.class), new Converter() {
            @Override
            public Object convert(Object value) {
                return new Double(((Number)value).doubleValue());
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, URI.class), new Converter() {
            @Override
            public Object convert(Object value) {
                String text = value.toString();
                try {
                    return new URI(text);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String.class, char[].class), new Converter() {
            @Override
            public Object convert(Object value) {
            	return ((String)value).toCharArray();
            }
        });
        CONVERSION_MAP.put(new ConversionKey(Object.class, String[].class), new Converter() {
            @Override
            public Object convert(Object value) {
            	return convertToStringArray(value);
            }
        });
        CONVERSION_MAP.put(new ConversionKey(String[].class,String.class), new Converter() {
            @Override
            public Object convert(Object value) {
            	return convertToString((String[]) value);
            }
        });
    }

    private TypeConversionSupport() {
    }

    @SuppressWarnings("rawtypes")
	public static <T> T valueOf(Map props,String key,Class<T> to){
    	if(props == null || key == null  || to == null){
    		return null;
    	}
    	Object value = props.get(key);
    	return convert(value, to);
    }
    @SuppressWarnings("rawtypes")
	public static String stringValueOf(Map props,String key){
    	return valueOf(props, key, String.class);
    }
    @SuppressWarnings("rawtypes")
	public static Integer intValueOf(Map props,String key){
    	return valueOf(props, key, Integer.class);
    }
    public static Converter lookupConverter(Class<?> from, Class<?> to) {
        // use wrapped type for primitives
        if (from.isPrimitive()) {
            from = convertPrimitiveTypeToWrapperType(from);
        }
        if (to.isPrimitive()) {
            to = convertPrimitiveTypeToWrapperType(to);
        }

        if (from.equals(to)) {
            return IDENTITY_CONVERTER;
        }

        return CONVERSION_MAP.get(new ConversionKey(from, to));
    }

    /**
     * Converts primitive types such as int to its wrapper type like
     * {@link Integer}
     */
    private static Class<?> convertPrimitiveTypeToWrapperType(Class<?> type) {
        Class<?> rc = type;
        if (type.isPrimitive()) {
            if (type == int.class) {
                rc = Integer.class;
            } else if (type == long.class) {
                rc = Long.class;
            } else if (type == double.class) {
                rc = Double.class;
            } else if (type == float.class) {
                rc = Float.class;
            } else if (type == short.class) {
                rc = Short.class;
            } else if (type == byte.class) {
                rc = Byte.class;
            } else if (type == boolean.class) {
                rc = Boolean.class;
            }
        }
        return rc;
    }


    public static String[] convertToStringArray(Object value) {
        if (value == null) {
            return null;
        }

        String text = value.toString();
        if (text == null || text.length() == 0) {
            return null;
        }

        StringTokenizer stok = new StringTokenizer(text, ",");
        final List<String> list = new ArrayList<String>();

        while (stok.hasMoreTokens()) {
            list.add(stok.nextToken());
        }

        String[] array = list.toArray(new String[list.size()]);
        return array;
    }

    public static String convertToString(String[] value) {
        if (value == null || value.length == 0) {
            return null;
        }

        StringBuffer result = new StringBuffer(String.valueOf(value[0]));
        for (int i = 1; i < value.length; i++) {
            result.append(",").append(value[i]);
        }

        return result.toString();
    }
	/**
	 * 将{@code value}转换为{@code right}指定的类型
	 * @param value
	 * @param left {@code value}的原类型,为{@code null}则使用{@code value.getClass()}代替
	 * @param right 目标类型
	 * @return R
	 */
	@SuppressWarnings("unchecked")
	public static final <L,R> R convert (L value,Class<L>left,Class<R> right){
		if(null == value){
            // lets avoid NullPointerException when converting to boolean for null values
            if (boolean.class.isAssignableFrom(right)) {
                return (R) Boolean.FALSE;
            }
			return null;
		}
		if(checkNotNull(right,"right is null").isInstance(value)){
			return right.cast(value);
		}
        Converter converter = lookupConverter(MoreObjects.firstNonNull(left,value.getClass()), right);
        checkArgument(converter != null,"Cannot convert from " + value.getClass()
        + " to " + right + " with value " + value);
        return (R) converter.convert(value);
	}
	/**
	 * 将{@code value}转换为{@code right}指定的类型
	 * @param value
	 * @param right 目标类型
	 * @return R
	 */
	public static final <R> R convert (Object value,Class<R> right){
		return convert(value, null, right);
	}

	/**
	 * 过滤 map 中 key 为 {@link String} 的项，并返回key为 {@link String} 的 {@link Map}<br>
	 * map为 {@code null}时返回空 {@link Map}对象
	 * @param map
	 * @since 2.4.0
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, Object> asStringKey(Map map) {
		if (null != map) {
			return Maps.newHashMap(Maps.filterKeys(map, Predicates.instanceOf(String.class)));
		}
		return Maps.newHashMap();
	}

	/**
	 * 过滤 map 中 key 为 {@link String} 的项，并返回{@link Properties}<br>
     * 如果 {@code map} 为{@code null},则返回空的{@link Properties}实例,
     * map为 {@code null}时返回空 {@link Properties}对象
	 * 
	 * @param map
	 * @since 2.4.0
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Properties asProperties(Map map) {
		if (map instanceof Properties) {
			return (Properties) map;
		}
		Properties properties = new Properties();
		if (null != map) {
			properties.putAll(Maps.filterEntries(map, new Predicate<Entry<Object, Object>>() {
				@Override
				public boolean apply(Entry<Object, Object> entry) {
					return null != entry.getKey() && null != entry.getValue();
				}
			}));
		}
		return properties;
	}
}
