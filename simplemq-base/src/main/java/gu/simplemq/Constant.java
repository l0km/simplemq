package gu.simplemq;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author guyadong
 *
 */
public interface Constant {
	public static final Logger logger = LoggerFactory.getLogger(Constant.class);

	public static final Charset UTF_8 = Charset.forName("UTF-8");
	/**
	 * 在REDIS中保存表记录时KEY的字符串分割符默认值
	 */
	public static final String DEFAULT_KEY_SEPARATOR = ":";
	
	/** milliseconds	 */
	public static final int DEFAULT_CONSUMER_CHECK_INTERVAL = 2000;

	/** 消息系统服务位置:URI */
	public static final String MQ_URI = "uri";
	/** 消息系统服务位置:主机名 */
	public static final String MQ_HOST = "host";
	/** 消息系统服务位置:端口号 */
	public static final String MQ_PORT = "port";
	public static final String PUBSUB_PREFIX = "pubsub.";
	/** 消息(订阅发布)系统服务位置:[boolean]是否为MQTT协议 */
	public static final String MQ_PUBSUB_MQTT = PUBSUB_PREFIX + "mqtt";
	/** 消息(订阅发布)系统服务位置:URI */
	public static final String MQ_PUBSUB_URI = PUBSUB_PREFIX + "uri";
	/** 消息(订阅发布)系统服务位置:主机名 */
	public static final String MQ_PUBSUB_HOST = PUBSUB_PREFIX + "host";
	/** 消息(订阅发布)系统服务位置:端口号 */
	public static final String MQ_PUBSUB_PORT = PUBSUB_PREFIX + "port";
	public static final String QUEUE_PREFIX = "queue.";
	/** 消息(队列)系统服务位置:URI */
	public static final String MQ_QUEUE_URI = QUEUE_PREFIX + "uri";
	/** 消息(队列)系统服务位置:主机名 */
	public static final String MQ_QUEUE_HOST = QUEUE_PREFIX + "host";
	/** 消息(队列)系统服务位置:端口号 */
	public static final String MQ_QUEUE_PORT = QUEUE_PREFIX + "port";
	
	public static final String WS_PREFIX = "ws.";
	/** 消息系统WebSocket服务位置:URI */
	public static final String MQ_WS_URI = WS_PREFIX + "uri";
	/** 消息系统WebSocket服务位置:主机名 */
	public static final String MQ_WS_HOST = WS_PREFIX + "host";
	/** 消息系统WebSocket服务位置:端口号 */
	public static final String MQ_WS_PORT = WS_PREFIX + "port";	
	
	/** 消息系统服务:连接用户名 */
	public static final String MQ_USERNAME = "username";
	/** 消息系统服务:连接用户名 */
	public static final String MQ_PASSWORD = "password";
	/** 消息系统服务:客户端ID */
	public static final String MQ_CLIENTID = "clientID";
	
	/** 消息系统服务:网络连接超时(毫秒) */
	public static final String MQ_TIMEOUT = "timeoutMills";

	/** MQTT 协议的URI SCHEMA */
	public static final String DEFAULT_MQ_HOST = "localhost";
	/** MQTT 协议的URI SCHEMA */
	public static final String MQTT_SCHEMA = "mqtt";
	/** MQTT SSL 协议的URI SCHEMA */
	public static final String MQTT_SSL_SCHEMA = "mqtt+ssl";
	
	/** 
	 * REDIS 协议的URI SCHEMA 
	 * @since 2.4.0
	 */
	public static final String REDIS_SCHEMA = "redis";
	/** 
	 * STOMP 协议的schema 
	 * @since 2.4.0
	 */
	public static final String STOMP_SCHEMA = "stomp";
	/** 
	 * OPENWIRE 协议的schema 
	 * @since 2.4.0
	 */
	public static final String OPENWIRE_SCHEMA = "openwire";
	public static final int DEFAULT_MQTT_PORT = 1883;
	public static final String DEFAULT_MQTT_CONNECTOR = MQTT_SCHEMA + "://" + DEFAULT_MQ_HOST + ":" + DEFAULT_MQTT_PORT;
	
	public static final int DEFAULT_AMQP_PORT = 5672;
	/** 
	 * AMQP 协议的URI SCHEMA 
	 */
	public static final String AMQP_SCHEMA = "amqp";
	public static final String DEFAULT_AMQP_CONNECTOR = AMQP_SCHEMA + "://" + DEFAULT_MQ_HOST + ":" + DEFAULT_AMQP_PORT;
	

}
