package gu.simplemq.pool;

import java.io.Closeable;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkState;

/**
 * 支持嵌套调用的消息系统连接对象管理基类,使用方法:<br>
 * 通过{@link #apply()} 和 {@link #free()}方法实现资源对象的申请和释放
 * @author guyadong
 *
 * @param <R> 资源池管理的资源类型
 */
public abstract class BaseMQPool<R> implements Closeable{
	protected volatile boolean closed = false;
	/** apply/free 嵌套计数 */
	private final ThreadLocal<AtomicInteger> tlsNestCount  = new ThreadLocal<AtomicInteger>(){
		@Override
		protected AtomicInteger initialValue() {
			return new AtomicInteger(0);
		}
	};

	/**
	 * 当前线程下的资源对象
	 */
	private final ThreadLocal<R> tlsResource = new ThreadLocal<R>();
	protected final Properties properties;
	protected final URI canonicalURI;

	protected BaseMQPool(Properties properties, URI canonicalURI) {
		this.properties = properties;
		this.canonicalURI = canonicalURI;
	}
	/**
	 * 从资源池获取一个资源对象<br>
	 * 对于调用此方法获取的实例，当不再使用时需要调用{@link #release(Object)}释放<br>
	 * @return 资源对象
	 */
	public abstract R borrow();
    
    /**
     * 将资源对象归还到资源池<br>
     * @param r 资源对象
     */
    public abstract void release(R r) ;
    
    /** 
	 * 申请当前线程使用的资源对象,不可跨线程使用<br>
	 * 使用完后调用 {@link #free()}释放对象
	 * @throws MQPoolException
	 */
	public final R apply() throws MQPoolException{
		R r = tlsResource.get();
		if(null == r){
			r = borrow();
			this.tlsResource.set(r);
		}
    	if(tlsNestCount.get() == null){
    		tlsNestCount.set(new AtomicInteger(0));
    	}
		tlsNestCount.get().incrementAndGet();
		return r;
	}
	/**
	 * 释放当前线程使用的资源
	 * 必须与{@link #apply()}配对使用
	 */
	public final void free(){
		R r = tlsResource.get();
		checkState(r != null && tlsNestCount.get() != null, "apply/free mismatch");		
		if(0 == tlsNestCount.get().decrementAndGet()){
			release(r);
			tlsResource.remove();
			tlsNestCount.remove();
		}
	}
	/**
	 * 返回当前线程池管理的资源对象位置
	 * @return 资源对象
	 * @since 2.4.0
	 */
	public URI getCanonicalURI() {
		return canonicalURI;
	}
	/**
	 * 获取连接池参数信息
	 * @since 2.4.0
	 */
	public Properties getProperties() {
		Properties props = new Properties();
		props.putAll(properties);
		return props;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName() + "@" + Integer.toHexString(hashCode())+" [properties=").append(properties).append(", canonicalURI=").append(canonicalURI)
				.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((canonicalURI == null) ? 0 : canonicalURI.hashCode());
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof BaseMQPool))
			return false;
		BaseMQPool other = (BaseMQPool) obj;
		if (canonicalURI == null) {
			if (other.canonicalURI != null)
				return false;
		} else if (!canonicalURI.equals(other.canonicalURI))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		return true;
	}
	/**
     * Has this pool instance been closed.
     * @return <code>true</code> when this pool has been closed.
     */
	public boolean isClosed() {
		return closed;
	}

	@Override
	public abstract void close();
	@SuppressWarnings("serial")
	public static class MQPoolException extends RuntimeException{
	
		public MQPoolException(Throwable cause) {
			super(cause);
		}
	}

}
