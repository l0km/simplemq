package gu.simplemq;

import java.util.Collection;

public interface IDequeProducer extends IProducer {
	/**
	 * 向指定的消息队列(频道)添加一个消息对象
	 * @param channel
	 * @param object
	 * @param offerLast 为true添加到队列末尾
	 */
	<T> void produce(Channel<T> channel, T object, boolean offerLast);
	/**
	 * 生产数据
	 * @param channel
	 * @param offerLast
	 * @param objects
	 * @see #produce(Channel, Object, boolean)
	 */
	<T> void produce(Channel<T> channel, boolean offerLast, @SuppressWarnings("unchecked") T... objects);
	/**
	 * 生产数据
	 * @param channel
	 * @param offerLast
	 * @param c
	 * @see #produce(Channel, Object, boolean)
	 */
	<T> void produce(Channel<T> channel, boolean offerLast, Collection<T> c);
	/**
	 * 设置默认的队列添加模式
	 * @param offerLast 为true默认添加到队列末尾
	 */
	void setOfferLast(boolean offerLast);
}
