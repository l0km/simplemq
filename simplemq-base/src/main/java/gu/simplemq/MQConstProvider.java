package gu.simplemq;

import java.util.Map;
import java.util.Properties;

public interface MQConstProvider{
	String getDefaultHost(); 
	int getDefaultPort();
	String getDefaultMQLocation();
	String getMainLocationName();
	String getMainUserName();
	String getMainPassword();
	String getMainClientID();
	String getMainTimeout();
	String getMainConnectTimeout();
	String[] getOptionalLocationNames();
	Properties getDefaultMQProperties();
	/**
	 * 返回所有支持的外部 scheme 与内部 scheme 映射关系
	 * @since 2.4.0
	*/
	Map<String, String> getNativeSchemeMap();
	/**
	 * 返回 MQ 实现类型
	 * @since 2.4.0
	*/
	MessageQueueType getImplType();
	/**
	 * 返回工厂实例支持的协议名称(小写),如{@code 'mqtt'}
	 * @since 2.4.0
	 */
	String getProtocol();
}
