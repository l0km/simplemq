package gu.simplemq.json;

/**
 * fastjson初始化接口,应用层可以实现此接口,
 * {@link BaseJsonEncoder}会在类初始化时以SPI(Service Provider Interface) 机制搜索{@link FastJsonInitializer}实例,
 * 并调用实例实现应用层的初始化动作.
 * @author guyadong
 *
 */
public interface FastJsonInitializer {
	void init();
}
