package gu.simplemq;

import java.util.Arrays;
import java.util.Collection;

import com.google.common.base.Function;

import gu.simplemq.pool.BaseMQPool;
/**
 * 基于{@link BaseMQSender}的{@link IProducer} 实现基类
 * @author guyadong
 *
 */
public abstract class BaseMQProducer<C> extends BaseMQSender<C> implements IProducer{
	public BaseMQProducer(BaseMQPool<C> poolLazy) {
		super(poolLazy);
	}
	
	@Override
	public final <T> void produce(Channel<T> channel, T object) {
		doProduce(channel, Arrays.asList(object));
	}

	@Override
	public final <T> void produce(Channel<T> channel, @SuppressWarnings("unchecked") T... objects) {
		if(objects != null){
			doProduce(channel, Arrays.asList(objects));
		}
	}	
	@Override
	public final <T> void produce(Channel<T> channel, Collection<T>c) {
		doProduce(channel, c);
	}
	@Override
	public IProducer withStringSerializer(Function<Object, String> stringSerializer) {
		super.setStringSerializer(stringSerializer);
		return this;
	}
}
