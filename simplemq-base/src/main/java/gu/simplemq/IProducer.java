package gu.simplemq;

import java.util.Collection;

import com.google.common.base.Function;

/**
 * 生产者模型接口(多队列)
 * @author guyadong
 *
 */
public interface IProducer {

	/**
	 * 根据默认的队列添加模式,向指定的消息队列(频道)添加一个消息对象
	 * @param channel
	 * @param object
	 */
	<T> void produce(Channel<T> channel, T object);

	/**
	 * 生产数据
	 * @param channel
	 * @param c
	 * @see #produce(Channel, Object)
	 */
	<T> void produce(Channel<T> channel, Collection<T>c);

	/**
	 * 生产数据
	 * @param channel
	 * @param objects
	 * @see #produce(Channel, Object) 
	 */
	<T> void produce(Channel<T> channel, @SuppressWarnings("unchecked") T... objects);

	IAdvisor getAdvisor();

	/**
	 * 指定发送数据的字符串序列化实例，默认序列化为JSON字符串,对当前实例有效
	 * @param stringSerializer 为{@code null}时恢复使用默认序列化实例
	 * @return 当前对象
	 * @since 2.3.11
	 */
	IProducer withStringSerializer(Function<Object, String> stringSerializer);
}