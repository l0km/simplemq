package gu.simplemq;

/**
 * 消息咨询接口
 * @author guyadong
 *
 */
public interface IAdvisor {
	/**
	 * 返回指定频道的消费者数量
	 * @param channelName 频道名
	 * @return 订阅频道的消费者数量，channel为{@code null}或空返回0
	 */
	public int consumerCountOf(String channelName);

	/**
	 * 返回指定频道的订阅者数量
	 * @param channelName 频道名
	 * @return 订阅频道的订阅者数量，channel为{@code null}或空返回0
	 */
	int subscriberCountOf(String channelName);
}
