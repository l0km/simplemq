package gu.simplemq;

public interface IMQContext {
	public MQPropertiesHelper getPropertiesHelper();
	public MessageQueueType getMessageQueueType();
	public String getClientImplType();
	public IMessageQueueFactory getMessageQueueFactory();
}
