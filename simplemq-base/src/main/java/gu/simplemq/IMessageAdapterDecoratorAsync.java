package gu.simplemq;

import java.util.concurrent.Executor;
import static com.google.common.base.Preconditions.*;
/**
 * 异步执行装饰类,可以用此类将一个{@link IMessageAdapterDecorator}实例改造为分发消息异步执行
 * @author guyadong
 *
 */
public class IMessageAdapterDecoratorAsync<T> extends IMessageAdapterDecorator<T> {

	private Executor executor;
	public IMessageAdapterDecoratorAsync(){
		super(null);
	}
	public IMessageAdapterDecoratorAsync(IMessageAdapter<T> adapter) {
		this(adapter,null);
	}

	/**
	 * @param adapter
	 * @param executor 异步执行消息分发的线程池对象
	 */
	public IMessageAdapterDecoratorAsync(IMessageAdapter<T> adapter, Executor executor) {
		super(adapter);
		this.executor = executor;
	}

	@Override
	protected Executor getExecutor() {
		return this.executor == null 
				? super.getExecutor() 
				: this.executor;
	}
	
	public static <T> IMessageAdapterDecoratorAsync<T> makeInstance(IMessageAdapter<T> adapter, Executor executor){
		if(adapter instanceof IMessageAdapterDecoratorAsync){
			return (IMessageAdapterDecoratorAsync<T>)adapter;
		}
		return new IMessageAdapterDecoratorAsync<T>(adapter,checkNotNull(executor,"executor is null"));
	}

}
