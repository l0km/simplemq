package gu.simplemq;


import gu.simplemq.exceptions.SmqNotFoundConnectionException;

/**
 * {@link IMQConnParameterSupplier}实例管理接口
 * @author guyadong
 *
 */
public interface IMessageQueueConfigManager {
	/**
	 * 自动查找可用的消息系统连接，返回第一个能建立有效连接的配置<br>
	 * @param timeoutMills 连接超时(毫秒),为{@code null}或小于等于0使用默认值
	 * @return 返回第一个能建立有效连接的配置,否则抛出异常
	 * @throws SmqNotFoundConnectionException 没有找到有效消息系统连接
	 */
	IMQConnParameterSupplier lookupMessageQueueConnect(Integer timeoutMills) throws SmqNotFoundConnectionException;
	/**
	 * 与{@link #lookupMessageQueueConnect(Integer)}功能相似,不同的时当没有找到有效连接时,不抛出异常,返回{@code null}
	 * @param timeoutMills 连接超时(毫秒),为{@code null}或小于等于0使用默认值
	 * @return 返回第一个能建立有效连接的配置,否则返回{@code null}
	 */
	IMQConnParameterSupplier lookupMessageQueueConnectUnchecked(Integer timeoutMills);
	/**
	 * 返回消息系统的实现类型(such as REDIS,ACTIVEMQ)
	 * @return 消息系统的实现类型
	 */
	MessageQueueType getImplType();

}
