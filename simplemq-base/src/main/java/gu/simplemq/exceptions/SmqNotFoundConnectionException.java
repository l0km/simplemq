package gu.simplemq.exceptions;

/**
 * 没有找到有效的消息系统服务异常
 * @author guyadong
 */
public class SmqNotFoundConnectionException extends SmqException {

	private static final long serialVersionUID = 1L;

	public SmqNotFoundConnectionException() {
	}

	public SmqNotFoundConnectionException(String message) {
		super(message);
	}

	public SmqNotFoundConnectionException(Throwable cause) {
		super(cause);
	}

	public SmqNotFoundConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
