package gu.simplemq.exceptions;

/**
 * @author guyadong
 *
 */
public class SmqRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SmqRuntimeException() {
	}

	public SmqRuntimeException(String message) {
		super(message);
	}

	public SmqRuntimeException(Throwable cause) {
		super(cause);
	}

	public SmqRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
