package gu.simplemq;

/**
 *  消息系统实现类型定义
 * @author guyadong
 *
 */
public enum MessageQueueType{
	/** 基于redis 实现 */REDIS,
	/** 基于activemq 实现 */ACTIVEMQ,
	/**
	 * 基于 eclipse paho 实现
	 * @since 2.4.0
	 */
	PAHO,
	/**
	 * 基于 eclipse paho 实现
	 * @since 2.4.0
	 */
	STOMP,
	/**
	 * 基于 Qpid Proton 实现
	 * @since 2.4.0
	 */
	PROTON
}