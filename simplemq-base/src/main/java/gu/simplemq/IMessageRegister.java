package gu.simplemq;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * (消息)频道注册订阅接口
 * @author guyadong
 *
 */
public interface IMessageRegister {

	/**
	 * 订阅频道
	 * @param channels 频道名列表,为null或空时订阅所有注册的频道
	 * @return 返回实际订阅的频道列表
	 */
	String[] subscribe(String... channels);

	/**
	 * 取消频道订阅
	 * @param channels 频道名列表,为null或空时取消订阅所有注册的频道
	 * @return 返回实际取消订阅的频道列表
	 */
	String[] unsubscribe(String... channels);

	/**
	 * 注册并订阅指定的频道
	 * @param channels
	 * @see #subscribe(String...)
	 * @return 返回实际注册的频道名列表
	 */
	Set<Channel<?>> register(Channel<?>... channels);

	/**
	 * 注册并订阅指定的频道,并在指定的时间后注销频道
	 * @param channel 频道名
	 * @param duration 频道订阅持续时间,>0有效
	 * @param unit 时间单位,不可为{@code null}
	 * @see #register(Channel...)
	 */
	void register( Channel<?>channel, long duration, TimeUnit unit);

	/**
	 * 取消订阅指定的频道,并注销频道
	 * @param channels
	 * @see #unsubscribe(String...)
	 * @return 返回实际注销的频道名列表
	 */
	Set<String> unregister(String... channels);

	/**
	 * 注销指定的频道
	 * @param channels 频道名列表
	 * @see #unregister(String...)
	 * @return 返回实际注销的频道名列表
	 */
	Set<String> unregister(Channel<?>... channels);

	/**
	 * 注册所有使用{@code messageAdapter}作处理器的频道
	 * @param messageAdapter
	 * @return 返回实际注销的频道名列表
	 */
	Set<String> unregister(final IMessageAdapter<?> messageAdapter);

	/**
	 * 返回注册的 {@link Channel}对象
	 * @param channel 频道名
	 */
	@SuppressWarnings("rawtypes")
	Channel getChannel(String channel);

	/**
	 * 返回当前订阅的所有频道名
	 */
	String[] getSubscribes();

}