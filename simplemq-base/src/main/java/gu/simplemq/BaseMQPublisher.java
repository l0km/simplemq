package gu.simplemq;

import java.util.Arrays;
import java.util.Collection;

import com.google.common.base.Function;

import gu.simplemq.pool.BaseMQPool;

/**
 * 
 * 基于{@link BaseMQSender}的{@link IPublisher} 抽象实现
 * @author guyadong
 *
 */
public abstract class BaseMQPublisher<C> extends BaseMQSender<C> implements IPublisher{
	public BaseMQPublisher(BaseMQPool<C> poolLazy) {
		super(poolLazy);
	}

	@Override
	public <T> long publish(Channel<T> channel, T object) {
		doProduce(channel, Arrays.asList(object));
		return 0L;
	}

	@Override
	public <T> void publish(Channel<T> channel, Collection<T> objects) {
		doProduce(channel,objects);
	}

	@Override
	public <T> void publish(Channel<T> channel, @SuppressWarnings("unchecked") T... objects) {
		if(objects != null){
			doProduce(channel, Arrays.asList(objects));
		}
	}
	@Override
	public IPublisher withStringSerializer(Function<Object, String> stringSerializer) {
		super.setStringSerializer(stringSerializer);
		return this;
	}
}
