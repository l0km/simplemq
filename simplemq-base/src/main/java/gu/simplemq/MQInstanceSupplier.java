package gu.simplemq;

import gu.simplemq.pool.BaseMQPool;

/**
 * 消息处理对象管理接口
 * 
 * @author guyadong
 * @since 2.4.0
 */
@SuppressWarnings("rawtypes")
public interface MQInstanceSupplier<POOL extends BaseMQPool> {
	/**
	 * 从连接池获取{@link IConsumer}实例
	 * 
	 * @param pool
	 * @see gu.simplemq.pool.BaseMQInstance#getInstance(Object)
	 */
	public IConsumer getConsumer(POOL pool);

	/**
	 * 从连接池获取{@link IProducer}实例
	 * 
	 * @param pool
	 * @see gu.simplemq.pool.BaseMQInstance#getInstance(Object)
	 */
	public IProducer getProducer(POOL pool);

	/**
	 * 从连接池获取{@link ISubscriber}实例
	 * 
	 * @param pool
	 * @see gu.simplemq.pool.BaseMQInstance#getInstance(Object)
	 */
	public ISubscriber getSubscriber(POOL pool);

	/**
	 * 从连接池获取{@link IPublisher}实例
	 * 
	 * @param pool
	 * @see gu.simplemq.pool.BaseMQInstance#getInstance(Object)
	 */
	public IPublisher getPublisher(POOL pool);
}
