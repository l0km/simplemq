package gu.simplemq;

import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * 订阅消息数据处理接口<br>
 * 对于接口，调用者会自动忽略上级接口中的{@link IMessageAdapter#onSubscribe(Object)}方法
 * 参见{@link ChannelDispatcher#dispatch(String, String)}
 * @author guyadong
 *
 * @param <T> 消息类型
 * @since 2.3.9
 */
public interface IMessageAdapterExt<T> extends IMessageAdapter<T> {
	/**
	 * 订阅消息处理,
	 * @param t 消息对象
	 * @param context 消息上下文对象对象
	 * @throws SmqUnsubscribeException 取消订阅当前频道异常指示
	 */
	public void onSubscribe(T t,SimplemqContext context) throws SmqUnsubscribeException ;
	public static abstract class  Adapter<T> implements IMessageAdapterExt<T>{

		@Override
		public void onSubscribe(T t) throws SmqUnsubscribeException {
			
		}		
	}
}
