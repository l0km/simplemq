package gu.simplemq.android.test;

import net.gdface.utils.JcifsUtil;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;

import jcifs.Address;
import jcifs.NameServiceClient;
import jcifs.NetbiosAddress;
import jcifs.context.SingletonContext;

public class Jcifs2Test {
	static final Logger logger = LoggerFactory.getLogger(Jcifs2Test.class);
	@Test
	public void test() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();

			NetbiosAddress[] addrs =	nsc.getNbtAllByAddress("chenpeng-pc");
			for(NetbiosAddress address:addrs){
			System.out.printf("%s\n",address.getHostName());
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test2() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();
			NetbiosAddress address = nsc.getNbtByName("chenpeng-pc");
			System.out.printf("%s\n",address.getHostName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test3() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();
			{
				// 提供的主机名返回所有绑定的地址对象
				Address[] addrs = nsc.getAllByName("chenpeng-pc", false);
				for(Address address : addrs){
					logger.info("{}",address);
				}
			}
			
			{
				System.out.println("============");
				// 根据提供的主机名解析为Address对象
				Address address = nsc.getByName("chenpeng-pc");
				logger.info("{}",address);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test4(){
		String host = "www.baidu.com";
		String change = JcifsUtil.changeHostIfLanhost(host);
		logger.info("{}:{}",host,change);
	}
	@Test
	public void test5() {
		try {
			String address = JcifsUtil.hostAddressOf("chenpeng-pc");
			System.out.printf("host ip:%s\n",address);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
