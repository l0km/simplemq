package gu.simplemq.mqtt;

import java.util.Date;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;

/**
 * @author guyadong
 *
 */
public class MqttPublisherTest implements MqttConstants{
	private static final String MQTT_HOST = "192.168.10.226";

	@Test
	public void test() throws InterruptedException {
		 Channel<Date> chat1 = new Channel<Date>("chat1",Date.class);
		IPublisher publisher = MqttFactory.getPublisher(MqttPoolLazys.getDefaultInstance());
		for(int i=0;i<100;++i){
			Date date = new Date();
			publisher.publish(chat1, date);
			logger.info(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
	}
	@SuppressWarnings({ "resource", "rawtypes", "unchecked" })
	@Test
	public void test2() throws Exception{
//		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_URI, PAHO_MQTT_SCHEMA + "://user:password@" + MQTT_HOST + ":" + DEFAULT_MQTT_PORT);
		final IMessageQueueFactory factory = new MessageQueueFactoryImpl().init((Map)m);
		IPublisher publisher = factory.getPublisher();
		Channel<String> c1 = new Channel<String>("chat1"){};
		Channel<String> c2 = new Channel<String>("chat2"){};
		for(int i=0;i<100;++i){
			Date date = new Date();
			publisher.publish(c1, "MQTT " + date.toString());
			publisher.publish(c2, "MQTT " + date.toString());
			logger.info(date.getTime() +" : " +date.toString());
			Thread.sleep(2000);
		}
		factory.close();
	}
}
