# simple Message Queue
simple Message Queue(以下简称simpleMQ)设计用来实现一个支持消息推送的中间件。

simpleMQ为不同消息协议提供统一的消息订阅/发布，队列生产/消费操作接口，致力于为应用层以协议透明的方式提供消息服务。

- 不同消息协议实现封装为独立的依赖库，应用层只需要像更换零件一样更换不同协议的依赖库，就可以实现消息协议的切换。而不必修改代码。

- 将订阅/发布模型抽象为ISubsciber/IPublisher接口，生产者/消费者模型抽象为IProducer/IConsumer接口。

- 支持常用的消息协议AMQP,MQTT,Stomp,OpenWire实现ISubsciber/IPublisher接口和IProducer/IConsumer接口

- 支持基于Redis的消息队列和频道订阅实现ISubsciber/IPublisher接口和IProducer/IConsumer接口
- 支持JMS消息接口框架
- API在线文档(javadoc) : [https://apidoc.gitee.com/l0km/simplemq](https://apidoc.gitee.com/l0km/simplemq)




| 子项目名称        | 消息协议                                                     | scheme        | 订阅/发布模型(ISubsciber/IPublisher) | 生产者/消费者模(IProducer/IConsumer) | 说明                                                         |
| ----------------- | ------------------------------------------------------------ | ------------- | ------------------------------------ | ------------------------------------ | ------------------------------------------------------------ |
| simplemq-base     |                                                              |               |                                      |                                      | 为所有消息协议实现提供抽象定义，如：ISubsciber/IPublisher接口，IProducer/IConsumer接口 |
| simplemq-amqp     | [AMQP 1.0](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=amqp) | amqp          | √                                    | √                                    | 基于[Apache Qpid Proton-j](https://qpid.apache.org/proton/index.html)实现AMQP协议的统一接口封装 |
| simplemq-openwire | [Wire Protocol](https://activemq.apache.org/components/classic/documentation/wire-protocol) | openwire      | √                                    | √                                    | 基于acitvemq的原生协议OpenWire实现AMQP协议的统一接口封装     |
| simplemq-stomp    | [Stomp](http://stomp.github.io/)                             | stomp         | √                                    | √                                    | 基于stilts-stomp-client实现Stomp协议的统一接口封装           |
| simplemq-mqtt     | [MQTT](http://mqtt.org/)                                     | mqtt,mqtt+ssl | √                                    |                                      | 基于[paho mqtt](https://eclipse.dev/paho/index.php?page=clients/python/index.php)实现MQTT协议的统一接口封装，仅支持订阅/发布模型,需要配合simplemq-amqp,simplemq-openwire或simplemq-stomp才能提供生产者/消费者模实现 |
| simplemq-redis    |                                                              | redis         | √                                    | √                                    | 基于redis的消息队列实现和频道订阅实现，这是最早实现的，但因为稳定性和可靠性不能保证，现已废弃，不推荐使用。 |
| simplemq-jms      |                                                              |               | √                                    | √                                    | 基于JMS的订阅/发布,生产/消费模型抽象实现，simplemq-amqp和simplemq-openwire都依赖此项目 |

## simplemq-base

### 消息队列

消息队列被设计成生产者/消费者模型

- 生产者抽象接口 [IProducer.java](simplemq-base/src/main/java/gu/simplemq/IProducer.java)

- 消费者抽象接口 [IConsumer.java](simplemq-base/src/main/java/gu/simplemq/IConsumer.java )

### 频道订阅

频道订阅被设计成订阅/发布模型

- 发布者接口 [IPublisher.java](simplemq-base/src/main/java/gu/simplemq/IPublisher.java )

- 订阅者接口 [ISubscriber.java](simplemq-base/src/main/java/gu/simplemq/ISubscriber.java)

### 消息系统统一封装接口

[IMessageQueueFactory](simplemq-base/src/main/java/gu/simplemq/IMessageQueueFactory.java)接口为不同协议提供生产者/消费者，发布者/订阅者实例定义统一接口，各协议实现会实现此接口，提供自己实现的ISubsciber/IPublisher接口，IProducer/IConsumer接口实例

### 发布示例

以AMQP协议为例，在项目中增加依赖库,如果使用其他协议，需要增加对应的依赖库

```xml
		<dependency>
			<groupId>com.gitee.l0km</groupId>
			<artifactId>simplemq-amqp</artifactId>
			<version>${simplemq.version}</version>
		</dependency>
```

测试代码：

```java
	public void test2() {
        /** 定义broker URI，如果需要用户密码验证，则需要在URI中定义用户名和密码 */
        ImmutableMap<String,String> m = ImmutableMap.of("uri", "amqp://localhost:5672");
        //////////////////////////////////////
        // 在URI中定义用户名和密码示例
		// ImmutableMap<String,String> m = ImmutableMap.of("uri", "amqp://user:password@localhost:5672");
        //////////////////////////////////////
        // 在username,password字段中定义用户名和密码
	    //ImmutableMap<String,String> m = ImmutableMap.of(
        //	"uri", "amqp://localhost:5672",
        //	"username","user",
        //	"password","password");
        //////////////////////////////////////        
        /** 根据输入的Map中 "uri" 字段的scheme,返回对应的IMessageQueueFactory实例，并使用Map初始化该实例 */ 
		// 目前支持的协议类型有: 
        // "amqp" ---- 由 simplemq-amqp 实现
        // "openwire" ----  由 simplemq-opwnwire 实现
        // "stomp" ---- 由 simplemq-stomp 实现
        // "mqtt","mqtt+ssl" ---- 由 simplemq-mqtt 实现
        // "redis" ---- 由 simplemq-redis 实现
        //////////////////////////////////////
		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m,false);
		try{
            /** 获取发布器 */
			IPublisher publisher = factory.getPublisher();
			Channel<String> c1 = new Channel<String>("chat1"){};
			for(int i=0;i<100;++i){
				Date date = new Date();
                /** 向频道发送数据 */
                publisher.publish(c1, "AMQP " + date.toString());
				logger.info(date.getTime() +" : " +date.toString());
				Thread.sleep(2000);
			}
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
```

完整代码参见

| 项目名称          | 测试代码                                                     |
| ----------------- | ------------------------------------------------------------ |
| simplemq-amqp     | [simplemq-amqp/src/test/java/gu/simplemq/proton/ProtonPublisherTest.java](simplemq-amqp/src/test/java/gu/simplemq/proton/ProtonPublisherTest.java) |
| simplemq-mqtt     | [simplemq-mqtt/src/test/java/gu/simplemq/mqtt/MqttPublisherTest.java](simplemq-mqtt/src/test/java/gu/simplemq/mqtt/MqttPublisherTest.java) |
| simplemq-openwire | [simplemq-openwire/src/test/java/gu/simplemq/activemq/ActivemqPublisherTest.java](simplemq-openwire/src/test/java/gu/simplemq/activemq/ActivemqPublisherTest.java) |
| simplemq-stomp    | [simplemq-stomp/src/test/java/gu/simplemq/stomp/StiltsPublisherTest.java](simplemq-stomp/src/test/java/gu/simplemq/stomp/StiltsPublisherTest.java) |
| simplemq-redis    | [simplemq-redis/src/test/java/gu/simplemq/redis/RedisPublisherTest.java](simplemq-redis/src/test/java/gu/simplemq/redis/RedisPublisherTest.java) |



### 订阅示例

以MQTT协议为例，在项目中增加依赖库,如果使用其他协议，需要增加对应的依赖库

```xml
		<dependency>
			<groupId>com.gitee.l0km</groupId>
			<artifactId>simplemq-mqtt</artifactId>
			<version>${simplemq.version}</version>
		</dependency>
```

测试代码：

```java
	public void test2() {
        /** 定义broker URI，如果需要用户密码验证，则需要在URI中定义用户名和密码 */
        ImmutableMap<String,String> m = ImmutableMap.of("uri", "mqtt://localhost:1883");
        //////////////////////////////////////
        // 在URI中定义用户名和密码示例
		// ImmutableMap<String,String> m = ImmutableMap.of("uri", "mqtt://user:password@localhost:1883");
        //////////////////////////////////////
        // 在username,password字段中定义用户名和密码
	    //ImmutableMap<String,String> m = ImmutableMap.of(
        //	"uri", "mqtt://localhost:1883",
        //	"username","user",
        //	"password","password");
        //////////////////////////////////////        
        /** 根据输入的Map中 "uri" 字段的scheme,返回对应的IMessageQueueFactory实例，并使用Map初始化该实例 */ 
		// 目前支持的协议类型有: 
        // "amqp" ---- 由 simplemq-amqp 实现
        // "openwire" ----  由 simplemq-opwnwire 实现
        // "stomp" ---- 由 simplemq-stomp 实现
        // "mqtt","mqtt+ssl" ---- 由 simplemq-mqtt 实现
        // "redis" ---- 由 simplemq-redis 实现
        //////////////////////////////////////
		IMessageQueueFactory factory = MessageQueueFactorys.getInitializedFactoryByUriScheme(m,false);
		try{
			logger.info("{}",factory.getHostAndPort());
			/** 获取订阅器实例 */
			ISubscriber subscriber = factory.getSubscriber();
			Channel<String> chat1 = new LogChannel("chat1");
            /** 订阅频道 */
			subscriber.register(chat1);
			waitquit();
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	class LogChannel extends Channel<String>{

		protected LogChannel(String name) {
			super(name);
		}
		@Override
		public void onSubscribe(String t) throws SmqUnsubscribeException {
			logger.info(name + " msg:" + t);
		}
	}
```

完整代码参见

| 项目名称          | 测试代码                                                     |
| ----------------- | ------------------------------------------------------------ |
| simplemq-amqp     | [simplemq-amqp/src/test/java/gu/simplemq/proton/ProtonSubscriberTest.java](simplemq-amqp/src/test/java/gu/simplemq/proton/ProtonSubscriberTest.java) |
| simplemq-mqtt     | [simplemq-mqtt/src/test/java/gu/simplemq/mqtt/MqttSubscriberTest.java](simplemq-mqtt/src/test/java/gu/simplemq/mqtt/MqttSubscriberTest.java) |
| simplemq-openwire | [simplemq-openwire/src/test/java/gu/simplemq/activemq/ActivemqSubscriberTest.java](simplemq-openwire/src/test/java/gu/simplemq/activemq/ActivemqSubscriberTest.java) |
| simplemq-stomp    | [simplemq-stomp/src/test/java/gu/simplemq/stomp/StiltsPublisherTest.java](simplemq-stomp/src/test/java/gu/simplemq/stomp/StiltsPublisherTest.java) |
| simplemq-redis    | [simplemq-redis/src/test/java/gu/simplemq/redis/RedisSubscriberTest.java](simplemq-redis/src/test/java/gu/simplemq/redis/RedisSubscriberTest.java) |

## simplemq-redis

基于redis和fastjson实现消息队列,频道订阅,K-V表，

### 消息队列

消息队列被设计成producer/consumer模型

[RedisProducer.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisProducer.java)

[RedisConsumer.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisConsumer.java )

### 频道订阅

频道订阅被设计成publisher/subscriber模型

[RedisPublisher.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisPublisher.java )

[RedisSubscriber.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisSubscriber.java )

### K-V表

K-V表

[RedisTable.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisTable.java )

### 实例化

[RedisFactory.java](simplemq-redis/src/main/java/gu/simplemq/redis/RedisFactory.java )用于创建上述的类的实例

### 调用示例

RedisConsumer示例

	public class TestRedisConsumer {
		private static final Logger logger = LoggerFactory.getLogger(TestRedisConsumer.class);
	
		@Test
		public void testRedisConsumer(){
			RedisConsumer consumer = RedisFactory.getConsumer(JedisPoolLazy.getDefaultInstance());
			Channel<String> list1 = new Channel<String>("list1",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","list1",t);
				}} );
			Channel<String> list2 = new Channel<String>("list2",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","list2",t);
				}} );
			Channel<String> list3 = new Channel<String>("list3",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","list3",t);
				}} );
			consumer.register(list1,list2);
			consumer.register(list3);
			consumer.unregister(list1);
		}
	}

RedisSubscriber示例


	public class TestRedisSubscriber {
		private static final Logger logger = LoggerFactory.getLogger(TestRedisSubscriber.class);
	
		@Test
		public void test() {
			RedisSubscriber subscriber = RedisFactory.getSubscriber(JedisPoolLazy.getDefaultInstance());
			Channel<String> chat1 = new Channel<String>("chat1",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","chat1",t);
				}} );
			Channel<String> chat2 = new Channel<String>("chat2",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","chat2",t);
				}} );
			Channel<String> chat3 = new Channel<String>("chat3",String.class,new IMessageAdapter<String>(){
	
				@Override
				public void onSubscribe(String t) throws SmqUnsubscribeException {
					logger.info("{}:{}","chat3",t);
				}} );
			subscriber.register(chat1,chat2);
			
			subscriber.register(chat3);
			subscriber.unsubscribe(chat1.name);
			subscriber.unsubscribe();
		}
	}

## License and Citation

simpleMQ is released under the [BSD 2-Clause license](LICENSE).


    @article{10km,
      Author = {Gu,YaDong 10km0811@sohu.com},
      Title = {simpleMQ: simple Message Queue},
      Year = {2017}
    }
