package gu.simplemq.stomp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLException;

import org.junit.Assert;
import org.junit.Test;
import org.projectodd.stilts.stomp.StompException;
import org.projectodd.stilts.stomp.StompMessage;
import org.projectodd.stilts.stomp.Subscription.AckMode;
import org.projectodd.stilts.stomp.client.ClientSubscription;
import org.projectodd.stilts.stomp.client.MessageHandler;
import org.projectodd.stilts.stomp.client.StompClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;
import gu.simplemq.exceptions.SmqUnsubscribeException;

/**
 * @author guyadong
 *
 */
public class StiltsSubscriberTest implements StompConstants{
	private static final Logger logger = LoggerFactory.getLogger(StiltsSubscriberTest.class);

    private static String broker = "stomp://127.0.0.1:61613";
//    private static String userName = "tuyou";
//    private static String passWord = "tuyou";
   

    private static StompClient connect() throws URISyntaxException, SSLException, InterruptedException, TimeoutException, StompException{
    	StompClient client = new StompClient( broker );
    	client.connect();
    	return client;
    }
    
    public static ClientSubscription sub(StompClient stompClient,String topic) throws StompException{
    	return  
		stompClient.subscribe( topic )
          .withMessageHandler( new MyCallback() )
          .withAckMode( AckMode.CLIENT_INDIVIDUAL )
//          .withAckMode( AckMode.CLIENT )

          .start();
    	
    }    
    

	private static void waitquit(){
		System.out.println("PRESS 'quit' OR 'CTRL-C' to exit");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		try{
			while(!"quit".equalsIgnoreCase(reader.readLine())){
			}
			//System.exit(0);			
		} catch (IOException e) {
	
		}finally {
	
		}
	}
	@Test
	public void test1() {
		StompClient stompClient = null;
		String prefix = "/topic/";
		try {
			stompClient = connect();
			ClientSubscription sub0 = sub(stompClient, prefix + "ActiveMQ.Advisory.Consumer.Topic.*");
			ClientSubscription sub1 = sub(stompClient, prefix + "chat1");
			ClientSubscription sub2 = sub(stompClient, prefix + "chat2");
			ClientSubscription sub3 = sub(stompClient, prefix + "chat3");
			waitquit();
			sub3.unsubscribe();
			sub2.unsubscribe();
			sub1.unsubscribe();
			sub0.unsubscribe();
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		} finally{
			try {
				stompClient.disconnect();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

	}
	@Test
	public void test2() {
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		final IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.STOMP).init(m);
		try{

			final Channel<String> c1 = new LogChannel("chat1");
			final Channel<String> c2 = new LogChannel("chat2");
			factory.getSubscriber().register(c1,c2);
			waitquit();
			factory.getSubscriber().unregister(c1,c2);
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void test3() {
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		final IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.STOMP).init(m);
		try{

			final Channel<String> c1 = new LogChannel("chat1");
			final Channel<String> c2 = new LogChannel("chat2");
			factory.getConsumer().register(c1,c2);
			waitquit();
			factory.getSubscriber().unregister(c1,c2);
			factory.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	static class MyCallback implements MessageHandler{
		@Override
		public void handle(StompMessage message) {
			logger.info("接收消息的主题 : " + message.getDestination());

			String msg = new String(message.getContentAsString());
			logger.info("msg:" + msg);
		}
		
	}
	class LogChannel extends Channel<String>{

		protected LogChannel(String name) {
			super(name);
		}
		@Override
		public void onSubscribe(String t) throws SmqUnsubscribeException {
			logger.info(name + " msg:" + t);				
		}
	}
}
