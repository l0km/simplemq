package gu.simplemq.stomp;

import static org.junit.Assert.*;
import static gu.simplemq.stomp.PropertiesHelper.SHELPER;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Constant;
import gu.simplemq.MQProperties;

public class PropertiesHelperTest implements Constant {

	@Test
	public void test() {
		{
			ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI,"stomp://localhost:61613");
			
			MQProperties props = SHELPER.asMQProperties(m);
			boolean connectable = StompPoolLazys.NAMED_POOLS.testConnect(props, null);
			logger.info("{} connectable {}", props.getLocation(),connectable);
			assertTrue(connectable);
		}
		{
		ImmutableMap<String, String> m = ImmutableMap.<String, String>of(MQ_URI,"stomp://dtalk.facelib.net:26417");
			MQProperties props = SHELPER.asMQProperties(m);
			boolean connectable = StompPoolLazys.NAMED_POOLS.testConnect(props, null);
			logger.info("{} connectable {}", props.getLocation(),connectable);
			assertFalse(connectable);
		}
	}

}
