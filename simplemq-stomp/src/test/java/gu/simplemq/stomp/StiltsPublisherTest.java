package gu.simplemq.stomp;

import static gu.simplemq.stomp.StiltsSubscriberTest.sub;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLException;

import org.junit.Test;
import org.projectodd.stilts.stomp.StompException;
import org.projectodd.stilts.stomp.StompMessages;
import org.projectodd.stilts.stomp.client.ClientSubscription;
import org.projectodd.stilts.stomp.client.StompClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IProducer;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.simplemq.MessageQueueType;
/**
 * @author guyadong
 *
 */
public class StiltsPublisherTest implements StompConstants{
	private static final Logger logger = LoggerFactory.getLogger(StiltsPublisherTest.class);
    private static String broker = "stomp://127.0.0.1:61613";
    private static StompClient connect() throws URISyntaxException, SSLException, InterruptedException, TimeoutException, StompException{
    	StompClient client = new StompClient( broker );
    	client.connect();
    	return client;
    }
	@Test
	public void test1() {
		StompClient stompClient = null;
		ClientSubscription sub0 = null;
		try {
			stompClient = connect();
			String prefix = "/topic/";

			sub0 = sub(stompClient, prefix + "ActiveMQ.Advisory.Consumer.Topic.*");

			for(int i=0;i<100;++i){
				Date date = new Date();
				stompClient.send(StompMessages.createStompMessage( "/topic/chat1", "msg-" + i + " " + date.toString() ));
				logger.info(date.getTime() +" : " +date.toString());
				Thread.sleep(2000);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		} finally{
			try {
				if(stompClient != null){
					stompClient.disconnect();
				}
				if(sub0 != null){
					sub0.unsubscribe();
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
		}

	}
	@Test
	public void test2() throws Exception{
		ImmutableMap<String,String> m = ImmutableMap.of(MQ_USERNAME,"user",MQ_PASSWORD,"password");
		final IMessageQueueFactory factory = MessageQueueFactorys.getFactory(MessageQueueType.STOMP).init(m);
		IPublisher publisher = factory.getPublisher();
		Channel<String> c1 = new Channel<String>("chat1"){};
		Channel<String> c2 = new Channel<String>("chat2"){};
		for(int i=0;i<100;++i){
			Date date = new Date();
			publisher.publish(c1, "STOMP " + date.toString());
			publisher.publish(c2, "STOMP " + date.toString());
			logger.info(date.getTime() +" : " +date.toString());
			logger.info("consumer count of chat1 {} consumer count of chat2 {}",
					publisher.getAdvisor().subscriberCountOf(c1.name),
					publisher.getAdvisor().subscriberCountOf(c2.name));
			Thread.sleep(2000);
		}
		factory.close();
	}
	
	@Test
	public void test3() throws Exception{
		ImmutableMap<String, Object> props = ImmutableMap.of(/*MQTT_clientId,(Object)"myidpub"*/);
		IMessageQueueFactory messageQueueFactory = MessageQueueFactorys.getFactory(MessageQueueType.STOMP).init(props);
		IProducer producer = messageQueueFactory.getProducer();
		
		Channel<String> c1 = new Channel<String>("chat1"){};
		Channel<String> c2 = new Channel<String>("chat2"){};
		for(int i=0;i<100;++i){
			Date date = new Date();
			producer.produce(c1, "STOMP " + date.toString());
			producer.produce(c2, "STOMP " + date.toString());
			logger.info(date.getTime() +" : " +date.toString());
			logger.info("consumer count of c1 {} consumer count of c2 {}",
					producer.getAdvisor().subscriberCountOf(c1.name),
					producer.getAdvisor().subscriberCountOf(c2.name));
			Thread.sleep(2000);
		}
		messageQueueFactory.close();
	}
}
