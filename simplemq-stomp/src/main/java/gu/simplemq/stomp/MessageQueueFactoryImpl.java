package gu.simplemq.stomp;

import gu.simplemq.BaseMessageQueueFactory;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.MQInstanceSupplier;
import gu.simplemq.pool.NamedMQPools;

/**
 * {@link IMessageQueueFactory}接口的STOMP实现类
 * @author guyadong
 *
 */
public final class MessageQueueFactoryImpl extends BaseMessageQueueFactory<StompPoolLazy>{

	private final MQInstanceSupplier<StompPoolLazy> mqInstanceFactory;
	private final NamedMQPools<StompPoolLazy> namedMQPools;
	MessageQueueFactoryImpl() {
		super(PropertiesHelper.SHELPER);
		this.mqInstanceFactory = StompFactory.MQ_INSTANCE_SUPPLIER;
		this.namedMQPools = StompPoolLazys.NAMED_POOLS;
	}

	@Override
	protected NamedMQPools<StompPoolLazy> getNamedMQPools() {
		return namedMQPools;
	}

	@Override
	protected MQInstanceSupplier<StompPoolLazy> getMqInstanceFactory() {
		return mqInstanceFactory;
	}
}
