package gu.simplemq.stomp;

import gu.simplemq.Constant;

/**
 * MQTT 常量定义
 * @author guyadong
 *
 */
public interface StompConstants extends Constant {
	
	public static final String STOMP_serverURI = "serverURI";
	
	public static final String HEADER_PREFIX = "header.";
	public static final String STOMP_username ="login";
	public static final String STOMP_password ="passcode";
	public static final String STOMP_clientId = "client-id";	
	
	public static final String DEFAULT_STOMP_HOST = "localhost";
	public static final int DEFAULT_STOMP_PORT = 61613;
	/** STOMP 协议的默认schema */
	public static final String DEFAULT_STOMP_SCHEMA = "stomp";
	
	public static final String DEFAULT_STOMP_URI = DEFAULT_STOMP_SCHEMA + "://" + DEFAULT_STOMP_HOST + ":" + DEFAULT_STOMP_PORT;
    
}
