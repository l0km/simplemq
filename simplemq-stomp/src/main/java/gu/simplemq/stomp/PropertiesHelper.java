package gu.simplemq.stomp;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MQPropertiesHelper;

public class PropertiesHelper extends MQPropertiesHelper {
	public static final PropertiesHelper SHELPER = new PropertiesHelper();

	PropertiesHelper() {
	}

	@Override
	public MQConstProvider getConstProvider() {
		return StompConstProvider.SPROVIDER;
	}
}
