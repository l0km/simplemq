package gu.simplemq.stomp;

/**
 * 基于STOMP的订阅接口实现<br>
 * @author guyadong
 *
 */
class StompSubscriber extends BaseStompDispatcher  {
	StompSubscriber(StompPoolLazy pool) {
		super(pool,"/topic/");
	}
}
