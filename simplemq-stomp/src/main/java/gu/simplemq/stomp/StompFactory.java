package gu.simplemq.stomp;

import gu.simplemq.MQInstanceFactory;

/**
 * 基于{@link MQInstanceFactory}实现 STOMP消息处理对象管理
 * @author guyadong
 *
 */
public class StompFactory extends MQInstanceFactory<StompConsumer,StompProducer,StompSubscriber,StompPublisher,StompPoolLazy>{
	public static final StompFactory MQ_INSTANCE_SUPPLIER = new StompFactory();
	StompFactory() {}
}
