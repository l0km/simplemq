package gu.simplemq.stomp;

import java.util.Map;
import gu.simplemq.MQProperties;
import gu.simplemq.pool.NamedMQPools;

/**
 * 基于名字管理的 {@link org.projectodd.stilts.stomp.client.StompClient}资源池（线程安全）<br>
 * @author guyadong
 *
 */
public class StompPoolLazys extends NamedMQPools<StompPoolLazy>{
	public static final StompPoolLazys NAMED_POOLS = new StompPoolLazys();
	StompPoolLazys() {
		super(PropertiesHelper.SHELPER);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public void checkConnect(Map input, Long timeoutMills) {
		MQProperties props = propertiesHelper.initParameters(input);
		if(timeoutMills != null && timeoutMills > 0){
			/** 定义 StompPoolLazy.timeoutMills 字段 */
			props.put("timeoutMills", timeoutMills);
		}
		checkConnect0(props);
	}
}
