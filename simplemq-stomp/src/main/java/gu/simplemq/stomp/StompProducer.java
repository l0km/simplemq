package gu.simplemq.stomp;

import org.projectodd.stilts.stomp.StompMessage;
import org.projectodd.stilts.stomp.StompMessages;
import org.projectodd.stilts.stomp.client.StompClient;

import gu.simplemq.BaseMQProducer;
import gu.simplemq.IAdvisor;
import gu.simplemq.IPublisher;
import gu.simplemq.MQRuntimeException;
import gu.simplemq.ZeroAdvisor;

/**
 * 
 * {@link IPublisher} STOMP实现
 * @author guyadong
 *
 */
public class StompProducer extends BaseMQProducer<StompClient> implements AutoCloseable{
	private final IAdvisor adivsor = new ZeroAdvisor();
	private final String destPrefix="/queue/"; 
	public StompProducer(StompPoolLazy poolLazy) {
		super(poolLazy);
	}
	@Override
	protected void doSend(StompClient c, String channel, Iterable<String> messages) throws Exception {
        for(String message : messages){
        	try {
        		StompMessage stompMessage = StompMessages.createStompMessage( 
        				destPrefix + channel, 
        				((StompPoolLazy)pool).getHeaders(),
        				message );
        		c.send(stompMessage);
			} catch (Exception e) {
				throw new MQRuntimeException(e);
			}
        }
	}
	
	@Override
	public void close() {
	}
	@Override
	public IAdvisor getAdvisor() {
		return adivsor;
	}


}
