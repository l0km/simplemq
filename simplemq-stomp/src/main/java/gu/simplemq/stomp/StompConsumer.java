package gu.simplemq.stomp;

/**
 * 基于STOMP的消费者接口实现<br>
 * @author guyadong
 *
 */
class StompConsumer extends BaseStompDispatcher  {
	StompConsumer(StompPoolLazy pool) {
		super(pool,"/queue/");
	}
}
