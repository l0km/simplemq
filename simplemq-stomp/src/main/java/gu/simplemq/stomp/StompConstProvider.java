package gu.simplemq.stomp;


import java.util.Map;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;

import gu.simplemq.MQConstProvider;
import gu.simplemq.MessageQueueType;

public class StompConstProvider implements MQConstProvider,StompConstants{
	public static final StompConstProvider SPROVIDER = new StompConstProvider();
	private static final String[] optionalLocationNames = new String[]{STOMP_serverURI};
	private static final Map<String, String> NATIVE_SCHEME_MAP = ImmutableMap.of(
			STOMP_SCHEMA, DEFAULT_STOMP_SCHEMA);
	/** 
	 * STOMP缺省连接参数<br>
	 * 这里没有使用guava的ImmutableMap，因为HashMap允许Value为null, ImmutableMap不允许 
	 **/
	@SuppressWarnings("serial")
	static final Properties DEFAULT_PARAMETERS = new Properties(){
		{
			put(STOMP_serverURI, DEFAULT_STOMP_URI);
		}
	};

	@Override
	public String getDefaultHost() {
		return DEFAULT_STOMP_HOST;
	}

	@Override
	public int getDefaultPort() {
		return DEFAULT_STOMP_PORT;
	}

	@Override
	public String getDefaultMQLocation() {
		return DEFAULT_STOMP_URI;
	}
	
	@Override
	public String getMainLocationName() {
		return STOMP_serverURI;
	}

	@Override
	public String getMainUserName() {
		return STOMP_username;
	}

	@Override
	public String getMainPassword() {
		return STOMP_password;
	}

	@Override
	public String getMainClientID() {
		return STOMP_clientId;
	}

	@Override
	public String getMainTimeout() {
		return "timeoutMills";
	}

	@Override
	public String getMainConnectTimeout() {
		return getMainTimeout();
	}

	@Override
	public String[] getOptionalLocationNames() {
		return optionalLocationNames;
	}

	@Override
	public Properties getDefaultMQProperties() {
		return StompConstProvider.DEFAULT_PARAMETERS;
	}

	@Override
	public Map<String, String> getNativeSchemeMap() {
		return NATIVE_SCHEME_MAP;
	}

	@Override
	public MessageQueueType getImplType() {
		return MessageQueueType.STOMP;
	}

	@Override
	public String getProtocol() {
		return STOMP_SCHEMA;
	}

}
